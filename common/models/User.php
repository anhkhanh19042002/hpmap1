<?php
namespace common\models;

use app\models\TrangThaiBanHang;
use backend\models\BangChamCong;
use backend\models\BangLuong;
use backend\models\CauHinh;
use backend\models\ChiaSeKhachHang;
use backend\models\DanhMuc;
use backend\models\GiaoDich;
use backend\models\KhoangGia;
use backend\models\LichSuDoanhThu;
use backend\models\LichSuTienVe;
use backend\models\NhuCauKhachHang;
use backend\models\SanPham;
use backend\models\ThongTinBanHang;
use backend\models\ThuongPhat;
use backend\models\TrangThaiChiaSeKhachHang;
use backend\models\TrangThaiNghi;
use backend\models\TrangThaiSanPham;
use backend\models\UserVaiTro;
use backend\models\Vaitrouser;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\IdentityInterface;

/**
 * @property int $id
 * @property string|null $anh_nguoi_dung
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email
 * @property string|null $auth_key
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $nguon
 * @property string|null $updated_at
 * @property string|null $password
 * @property string|null $nhom
 * @property string|null $hoten
 * @property string|null $dien_thoai
 * @property string|null $cmnd
 * @property string|null $dia_chi
 * @property int|null $active
 * @property int|null $user_id
 * @property int|null $vi_dien_tu
 * @property string|null $ngay_sinh
 * @property string|null $nhu_cau_quan
 * @property string|null $nhu_cau_huong
 * @property float|null $nhu_cau_gia_tu
 * @property float|null $nhu_cau_gia_den
 * @property float|null $nhu_cau_dien_tich_tu
 * @property float|null $nhu_cau_dien_tich_den
 * @property string|null $ghi_chu
 * @property string|null $khoang_gia
 * @property float|null $he_so_luong
 * @property string|null $ngay_nghi
 * @property int|null $uid
 * @property int|null $parent_id
 * @property int|null $so_bai_viet_con_lai
 * @property int|null $code_xac_thuc
 * @property int|null $is_xac_thuc
 * @property int|null $send_sms
 * @property int|null $luot_click_sdt
 *
 * @property BangChamCong[] $bangChamCongs
 * @property BangLuong[] $bangLuongs
 * @property ChiaSeKhachHang[] $chiaSeKhachHangs
 * @property ChiaSeKhachHang[] $chiaSeKhachHangs0
 * @property ChiaSeKhachHang[] $chiaSeKhachHangs1
 * @property ChiaSeKhachHang[] $chiaSeKhachHangs2
 * @property LichSuDoanhThu[] $lichSuDoanhThus
 * @property LichSuDoanhThu[] $lichSuDoanhThus0
 * @property LichSuDoanhThu[] $lichSuDoanhThus1
 * @property User $parent
 * @property SanPham[] $sanPhams
 * @property SanPham[] $sanPhams0
 * @property SanPham[] $sanPhams1
 * @property SanPham[] $sanPhams2
 * @property SanPham[] $sanPhams3
 * @property SanPham[] $sanPhams4
 * @property ThongTinBanHang[] $thongTinBanHangs
 * @property ThongTinBanHang[] $thongTinBanHangs0
 * @property ThongTinBanHang[] $thongTinBanHangs1
 * @property ThuongPhat[] $thuongPhats
 * @property TrangThaiBanHang[] $trangThaiBanHangs
 * @property TrangThaiChiaSeKhachHang[] $trangThaiChiaSeKhachHangs
 * @property TrangThaiSanPham[] $trangThaiSanPhams
 * @property User $user
 * @property User[] $users
 * @property User[] $users0
 * @property Vaitrouser[] $vaitrousers
 */
class User extends ActiveRecord implements IdentityInterface
{
    public $ngay_xem;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    const KHACH_HANG = 'Khách hàng';
    const THANH_VIEN = 'Thành viên';
    const GIAM_DOC = 'Giám đốc';
    const TRUONG_PHONG = 'Trưởng phòng';
    const NHAN_VIEN = 'Nhân viên';

    public static function tableName()
    {
        return 'vh_user';
    }

    public function rules()
    {
        return [
            [['status', 'active', 'user_id', 'so_bai_viet_con_lai', 'code_xac_thuc', 'is_xac_thuc', 'send_sms', 'luot_click_sdt'], 'integer'],
            [['created_at', 'updated_at','nhom', 'nhu_cau_quan', 'nhu_cau_huong', 'vi_dien_tu'], 'safe'],
            [['ngay_sinh','ghi_chu','khoang_gia', 'ngay_xem', 'parent_id'], 'safe'],
            [['nhu_cau_gia_tu', 'nhu_cau_gia_den', 'nhu_cau_dien_tich_tu', 'nhu_cau_dien_tich_den', 'he_so_luong'], 'number'],
            [['username', 'password_hash', 'email', 'password', 'hoten', 'nguon'], 'safe'],
            [['password_reset_token'], 'string', 'max' => 45],
            [['auth_key'], 'string', 'max' => 32],
            [['dien_thoai', 'cmnd'], 'safe'],
            [['dia_chi'], 'safe'],
            [['anh_nguoi_dung'], 'string'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function validateDouble($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = User::find()->andFilterWhere(["{$attribute}" => $this->$attribute])
                ->andFilterWhere(['status' => [self::STATUS_ACTIVE], 'active' => 1]);
            if(!$this->isNewRecord){
                $user->andFilterWhere(['<>', 'id', $this->id]);
            }

            if(!is_null($user->one())){
                $this->addError($attribute, "{$this->getAttributeLabel($attribute)} đã tồn tại");
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Tài khoản',
            'password_hash' => 'Mật khẩu',
            'status' => 'Trạng thái',
            'vaitro' => 'Vai trò',
            'hoten' => 'Họ tên',
            'ngay_xem' => 'Ngày xem',
            'so_bai_viet_con_lai' => 'So Bai Viet Con Lai',
            'code_xac_thuc' => 'Code Xac Thuc',
            'is_xac_thuc' => 'Is Xac Thuc',
            'send_sms' => 'Send Sms',
            'luot_click_sdt' => 'Luot Click Sdt',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeSave($insert)
    {
        if(is_array($this->nhu_cau_quan)){
            $quans = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['in', 'id', $this->nhu_cau_quan])->all(), 'id', 'name');
            $this->nhu_cau_quan = implode(', ', $quans);
        }
        if(is_array($this->nhu_cau_huong))
            $this->nhu_cau_huong = implode(', ', $this->nhu_cau_huong);

        $this->ngay_sinh = myAPI::convertDateSaveIntoDb($this->ngay_sinh);

        if ($insert){
            $this->created_at = date("Y-m-d H:i:s");
            if ($this->nhom == User::KHACH_HANG) {
                $this->user_id = \Yii::$app->user->id;
            } else {
                $this->is_xac_thuc = 1;
                $this->code_xac_thuc = rand(10000, 99999);
                $this->send_sms = 1;
            }
            $cau_hinh_he_so_luong = CauHinh::findOne(['name' => CauHinh::HE_SO_LUONG]);
            $this->he_so_luong = $cau_hinh_he_so_luong->content;
            $this->generateAuthKey();
        }
        else {
            $this->updated_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (isset($_POST['Vaitrouser'])) {
            $vaitro = Vaitrouser::findAll(['user_id' => $this->id]);
            foreach ($vaitro as $item) {
                $item->delete();
            }

            foreach ($_POST['Vaitrouser'] as $item) {
                $vaitronguoidung = new Vaitrouser();
                $vaitronguoidung->vaitro_id = $item;
                $vaitronguoidung->user_id = $this->id;
                if (!$vaitronguoidung->save()) {
                    throw new HttpException(500, Html::errorSummary($vaitronguoidung));
                }
            }
        }

        $arr = [];
        $cau_hinh = CauHinh::findOne(['ghi_chu'=>'khoang_gia']);
        $arrCauHinh  = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        foreach ($arrCauHinh as $index =>  $item){
            $arrGia = explode(' - ', $item);
            if ($this->nhu_cau_gia_tu >= (float)$arrGia[0] && $this->nhu_cau_gia_tu < (float)$arrGia[1]){
                $arr[] = $index;
            }
            if ($this->nhu_cau_gia_den != ''){
                if ($this->nhu_cau_gia_den >= (float)$arrGia[0] && $this->nhu_cau_gia_den < (float)$arrGia[1]){
                    $arr[] = $index;
                }
            }
        }
        $arrKhoangGia = [];
        if (count($arr) == 1){
            $arrKhoangGia[] = trim($arrCauHinh[$arr[0]]);
        }elseif (count($arr) == 2){
            for ( $i = $arr[0];$i <= $arr[1] ; $i++){
                $arrKhoangGia[] = trim($arrCauHinh[$i]);
            }
        }

        $khoang_gia = implode(', ',$arrKhoangGia);
        $this->updateAttributes(['khoang_gia'=>$khoang_gia]);

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function isAccess($arrRoles){
        return !is_null(Vaitrouser::find()->andFilterWhere(['in', 'vaitro', $arrRoles])->andFilterWhere(['user_id' => Yii::$app->user->getId()])->one());
    }

    public function getChiaSeKhachHangs()
    {
        return $this->hasMany(ChiaSeKhachHang::className(), ['khach_hang_id' => 'id']);
    }

    public function getChiaSeKhachHangs0()
    {
        return $this->hasMany(ChiaSeKhachHang::className(), ['nguoi_chia_se_id' => 'id']);
    }

    public function getChiaSeKhachHangs1()
    {
        return $this->hasMany(ChiaSeKhachHang::className(), ['nguoi_nhan_id' => 'id']);
    }

    public function getChiaSeKhachHangs2()
    {
        return $this->hasMany(ChiaSeKhachHang::className(), ['user_id' => 'id']);
    }

    public function getSanPhams()
    {
        return $this->hasMany(SanPham::className(), ['khach_hang_id' => 'id']);
    }

    public function getSanPhams0()
    {
        return $this->hasMany(SanPham::className(), ['nhan_vien_ban_id' => 'id']);
    }

    public function getSanPhams1()
    {
        return $this->hasMany(SanPham::className(), ['user_id' => 'id']);
    }

    public function getSanPhams2()
    {
        return $this->hasMany(SanPham::className(), ['nhan_vien_phu_trach_id' => 'id']);
    }

    public function getSanPhams3()
    {
        return $this->hasMany(SanPham::className(), ['nguoi_tao_id' => 'id']);
    }

    public function getThongBaos()
    {
        return $this->hasMany(ThongBao::className(), ['user_id' => 'id']);
    }

    public function getThongBaos0()
    {
        return $this->hasMany(ThongBao::className(), ['nguoi_nhan_id' => 'id']);
    }

    public function getThongTinBanHangs()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['user_id' => 'id']);
    }

    public function getThongTinBanHangs0()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['nguoi_ban_id' => 'id']);
    }

    public function getThongTinBanHangs1()
    {
        return $this->hasMany(ThongTinBanHang::className(), ['khach_hang_id' => 'id']);
    }

    public function getTrangThaiBanHangs()
    {
        return $this->hasMany(TrangThaiBanHang::className(), ['user_id' => 'id']);
    }

    public function getTrangThaiChiaSeKhachHangs()
    {
        return $this->hasMany(TrangThaiChiaSeKhachHang::className(), ['user_id' => 'id']);
    }

    public function getTrangThaiSanPhams()
    {
        return $this->hasMany(TrangThaiSanPham::className(), ['user_id' => 'id']);
    }
    public function getTrangThaiNghis()
    {
        return $this->hasMany(TrangThaiNghi::className(), ['user_id' => 'id']);
    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_id' => 'id']);
    }

    public function getVaitrousers()
    {
        return $this->hasMany(Vaitrouser::className(), ['user_id' => 'id']);
    }

    public function beforeDelete()
    {
        Vaitrouser::deleteAll(['user_id' => $this->id]);
        GiaoDich::deleteAll(['nguoi_nhan_id' => $this->id]);
        GiaoDich::deleteAll(['nguoi_thuc_hien_giao_dich_id' => $this->id]);
        User::updateAll(['parent_id' => 1], ['parent_id' => $this->id]);
        ChiaSeKhachHang::deleteAll(['nguoi_chia_se_id' => $this->id]);
        ChiaSeKhachHang::deleteAll(['nguoi_nhan_id' => $this->id]);
        ChiaSeKhachHang::deleteAll(['user_id' => $this->id]);
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public static function isViewAll(){
        // Nếu là admin hoặc Giám đốc thì nhìn đc hết mọi đề nghị, ngược lại chỉ xem đc đề nghị của mình
        $user_vaitro = UserVaiTro::find()->andFilterWhere(['id' => Yii::$app->user->id])
                                        ->andFilterWhere(['status' => 10])
                                        ->andFilterWhere(['like', 'vai_tro', self::GIAM_DOC])
                                        ->one();
        return (Yii::$app->user->id == 1 || !is_null($user_vaitro));
    }

    public static function isUpdateAll(){
        // Nếu là admin hoặc Giám đốc hoặc Trưởng phòng thì đc giao sp, huỷ, sửa, xoá hết mọi đề nghị, ngược lại chỉ xem đc đề nghị của mình
        $user_vaitro = UserVaiTro::find()->andFilterWhere(['id' => Yii::$app->user->id])
            ->andFilterWhere(['status' => 10])
            ->andFilterWhere(['or',['like', 'vai_tro', self::GIAM_DOC],['like','vai_tro',self::TRUONG_PHONG]])
            ->one();
        return (Yii::$app->user->id == 1 || !is_null($user_vaitro));
    }

    public function getHeSoLuongTheoNgay($thoi_gian){
        $bang_cham_cong = BangChamCong::findOne(['nhan_vien_id' => $this->id, 'ngay' => $thoi_gian]);
        $he_so='';
        if(isset($bang_cham_cong->sang)&&isset($bang_cham_cong->chieu)&&isset($bang_cham_cong->ly_do_nghi))
        {
            $he_so=BangChamCong::CO_PHEP;
        }
        else
        if(isset($bang_cham_cong->sang)&&isset($bang_cham_cong->chieu))
        {
            $he_so=BangChamCong::CA_NGAY;
        }
        else
        if(isset($bang_cham_cong->sang))
        {
            $he_so=BangChamCong::SANG;
        }
        else
        if(isset($bang_cham_cong->chieu))
        {
            $he_so=BangChamCong::CHIEU;
        }
        else
        if(isset($bang_cham_cong->ly_do_nghi))
        {
            $he_so=BangChamCong::NGHI;
        }

        return $he_so;
    }
    public function LiDoNGhi($thoi_gian){
        $bang_cham_cong = BangChamCong::findOne(['nhan_vien_id' => $this->id, 'ngay' => $thoi_gian]);
        if(isset($bang_cham_cong))
        {
            return $bang_cham_cong->ly_do_nghi;
        }
    }
    public static function  getSoNgayNghi($thang,$id)
    {
        $so_lan_nghi= TrangThaiNghi::findOne(['user_id'=>$id,'thang'=>$thang]);
        if(!isset($so_lan_nghi))
        {
            $so_lan_nghi= new TrangThaiNghi();
            $so_lan_nghi->so_lan_nghi_con_lai=1;
            $so_lan_nghi->thang= date('m');
            $so_lan_nghi->user_id=$id;
            $so_lan_nghi->save();
        }
        return $so_lan_nghi->so_lan_nghi_con_lai;
    }
    public function getDiemDanhSang($thoi_gian){
        $bang_cham_cong = BangChamCong::findOne(['nhan_vien_id' => $this->id, 'ngay' => $thoi_gian]);
        if(is_null($bang_cham_cong)){
            return '';
        }else{
            return $bang_cham_cong->sang;
        }
    }
    public function getDiemDanhChieu($thoi_gian){
        $bang_cham_cong = BangChamCong::findOne(['nhan_vien_id' => $this->id, 'ngay' => $thoi_gian]);
        if(is_null($bang_cham_cong)){
            return '';
        }else{
            return $bang_cham_cong->chieu;
        }
    }
    public function getNghi($thoi_gian){
        $bang_cham_cong = BangChamCong::findOne(['nhan_vien_id' => $this->id, 'ngay' => $thoi_gian]);
        if(is_null($bang_cham_cong)){
            return '';
        }else{
            return $bang_cham_cong->ly_do_nghi;
        }
    }
    public function getNgayCongTheoThang($month, $year){
        $tu_ngay = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $den_ngay = date('Y-m-t', strtotime($year.'-'.$month.'-1'));
        $ngay_cong = 0;

        $bang_cham_congs =BangChamCong::find()
            ->andFilterWhere(['nhan_vien_id' => $this->id])
            ->andFilterWhere(['>=', 'ngay', $tu_ngay])
            ->andFilterWhere(['<=', 'ngay', $den_ngay])->all();

        if(!empty($bang_cham_congs)) {
            foreach ($bang_cham_congs as $bang_cham_cong) {
                $ngay_cong += $bang_cham_cong->he_so;
            }
        }

        return $ngay_cong;
    }
    public static function UserVaiTro($id){
        $vai_tro = UserVaiTro::findOne(['vai_tro' => $id,]);
        return $vai_tro->vai_tro;
    }
    public function getThuongPhatTheoThang($month, $year){
        $thuong = 0;
        $phat = 0;
        $hoa_hong = 0;
        $da_lay = 0;
        $tu_ngay = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $den_ngay = date('Y-m-t', strtotime($year.'-'.$month.'-1'));
        $thuong_phats = ThuongPhat::find()
            ->andFilterWhere(['nhan_vien_id' => $this->id])
            ->andFilterWhere(['>=', 'ngay_ap_dung', $tu_ngay])
            ->andFilterWhere(['<=', 'ngay_ap_dung', $den_ngay])->all();

        if(!empty($thuong_phats)) {
            foreach ($thuong_phats as $thuong_phat) {
                /** @var $thuong_phat ThuongPhat */
                if ($thuong_phat->type == ThuongPhat::THUONG) {
                    if ($thuong_phat->kieu == ThuongPhat::NHAP_HANG || $thuong_phat->kieu == ThuongPhat::BAN_HANG || $thuong_phat->kieu == ThuongPhat::CHUYEN_KHACH){
                        $hoa_hong += $thuong_phat->chi_phi;
                        $da_lay += $thuong_phat->thuong_ban_da_lay + $thuong_phat->thuong_nhap_da_lay + $thuong_phat->thuong_chuyen_khach_da_lay;
                    }else{
                        $thuong += $thuong_phat->chi_phi;
                    }
                } else {
                    $phat += $thuong_phat->chi_phi;
                }
            }
        }

        return [
            'thuong' => $thuong,
            'phat' => $phat,
            'hoa_hong'=> $hoa_hong,
            'da_lay' => $da_lay,
        ];
    }

    public function getSanPhamTheoThang($month, $year){
        $so_can = 0;
        $tu_ngay = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $den_ngay = date('Y-m-t', strtotime($year.'-'.$month.'-1'));
        $lsDoanhThus = LichSuDoanhThu::find()->andFilterWhere(['nguoi_ban_id' => $this->id])
            ->andFilterWhere(['active' => 1])
            ->andFilterWhere(['>=', 'ngay_ban', $tu_ngay])
            ->andFilterWhere(['<=', 'ngay_ban', $den_ngay])->all();

//        $so_cans = ThongTinBanHang::find()->andFilterWhere(['nguoi_ban_id' => $this->id])
//            ->andFilterWhere(['trang_thai' => ThongTinBanHang::DA_MUA])
//            ->andFilterWhere(['>=', 'ngay_ban', $tu_ngay])
//            ->andFilterWhere(['<=', 'ngay_ban', $den_ngay])
//            ->andFilterWhere(['active'=>1])->all();

        if(!empty($lsDoanhThus)) {
            foreach ($lsDoanhThus as $item) {
                $so_can++;
            }
        }
        return [
            'so_can' => $so_can,
        ];
    }

    public function getGhiChu($month, $year){
        $ghi_chu = '';
        $tu_ngay = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $den_ngay = date('Y-m-t', strtotime($year.'-'.$month.'-1'));
        $thuong_phats = ThuongPhat::find()
            ->andFilterWhere(['nhan_vien_id' => $this->id])
            ->andFilterWhere(['>=', 'ngay_ap_dung', $tu_ngay])
            ->andFilterWhere(['<=', 'ngay_ap_dung', $den_ngay])->all();
        if(!empty($thuong_phats)) {
            foreach ($thuong_phats as $thuong_phat) {
                /** @var $thuong_phat ThuongPhat */
                if ($thuong_phat->ghi_chu != '' & $thuong_phat->ghi_chu != null)
                    $ghi_chu .= $thuong_phat->ghi_chu.'<br>';
            }
        }
        return [
            'ghi_chu' => $ghi_chu,
        ];
    }

    public function getDoanhThuHoaHong($month, $year){
        $doanh_thu_hoa_hong = 0;
        $hoa_hong_ve_cong_ty = 0;
        $hoa_hong_chua_lay = 0;
        $hoa_hong_ve_cong_ty_da_lay =0;
        $tu_ngay = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $den_ngay = date('Y-m-t', strtotime($year.'-'.$month.'-1'));

        $lsDoanhThus = LichSuDoanhThu::find()->andFilterWhere(['nguoi_ban_id' => $this->id])
            ->andFilterWhere(['>=', 'ngay_ban', $tu_ngay])
            ->andFilterWhere(['<=', 'ngay_ban', $den_ngay])->all();

        if(!empty($lsDoanhThus)){
            foreach ($lsDoanhThus as $item) {
                /** @var $item LichSuDoanhThu */
                if($this->id === 1){
                    $doanh_thu_hoa_hong += $item->doanh_thu_hoa_hong;
                    $hoa_hong_ve_cong_ty += $item->doanh_thu_hoa_hong;
                    $hoa_hong_chua_lay += $item->doanh_thu_hoa_hong - $item->doanh_thu_da_nhan;
                    $hoa_hong_ve_cong_ty_da_lay += $item->doanh_thu_da_nhan;
                }else{
                    $doanh_thu_hoa_hong += $item->doanh_thu_hoa_hong;
                    $hoa_hong_ve_cong_ty += $item->doanh_thu_hoa_hong - $item->hoa_hong_chuyen_khach - $item->hoa_hong_nhap - $item->hoa_hong_ban;
                    $hoa_hong_chua_lay += $item->doanh_thu_hoa_hong - $item->doanh_thu_da_nhan;
                    $hoa_hong_ve_cong_ty_da_lay += ($item->doanh_thu_da_nhan - $item->hoa_hong_nhap_da_nhan - $item->hoa_hong_chuyen_khach_da_nhan - $item->hoa_hong_ban_da_nhan);
                }
            }
        }

//        $san_phams = ThongTinBanHang::find()->andFilterWhere(['nguoi_ban_id' => $this->id])
//            ->andFilterWhere(['trang_thai' => ThongTinBanHang::DA_MUA])
//            ->andFilterWhere(['>=', 'ngay_ban', $tu_ngay])
//            ->andFilterWhere(['<=', 'ngay_ban', $den_ngay])
//            ->andFilterWhere(['active'=>1])->all();
//
//        if(!empty($san_phams)) {
//            foreach ($san_phams as $item) {
//                /** @var $item ThongTinBanHang */
//                $doanh_thu_hoa_hong += $item->sanPham->doanh_thu_hoa_hong;
//                $hoa_hong_ve_cong_ty += $item->sanPham->loi_nhuan;
//                $hoa_hong_chua_lay += $item->sanPham->doanh_thu_con_lai;
//                $hoa_hong_ve_cong_ty_da_lay += ($item->sanPham->doanh_thu_da_lay - $item->sanPham->da_thanh_toan_nhap_hang - $item->sanPham->da_thanh_toan_ban_hang - $item->sanPham->da_lay_hoa_hong_chuyen_khach);
//            }
//        }
        return [
            'doanh_thu_hoa_hong' => $doanh_thu_hoa_hong,
            'hoa_hong_ve_cong_ty' => $hoa_hong_ve_cong_ty,
            'hoa_hong_chua_lay' => $hoa_hong_chua_lay,
            'hoa_hong_ve_cong_ty_chua_lay' => $hoa_hong_ve_cong_ty - $hoa_hong_ve_cong_ty_da_lay,
        ];
    }

    public function backupgetDoanhThuHoaHong($month, $year){
        $doanh_thu_hoa_hong = 0;
        $hoa_hong_ve_cong_ty = 0;
        $hoa_hong_chua_lay = 0;
        $hoa_hong_ve_cong_ty_da_lay =0;
        $tu_ngay = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $den_ngay = date('Y-m-t', strtotime($year.'-'.$month.'-1'));

        $san_phams = ThongTinBanHang::find()->andFilterWhere(['nguoi_ban_id' => $this->id])
            ->andFilterWhere(['trang_thai' => ThongTinBanHang::DA_MUA])
            ->andFilterWhere(['>=', 'ngay_ban', $tu_ngay])
            ->andFilterWhere(['<=', 'ngay_ban', $den_ngay])
            ->andFilterWhere(['active'=>1])->all();

        if(!empty($san_phams)) {
            foreach ($san_phams as $item) {
                /** @var $item ThongTinBanHang */
                $doanh_thu_hoa_hong += $item->sanPham->doanh_thu_hoa_hong;
                $hoa_hong_ve_cong_ty += $item->sanPham->loi_nhuan;
                $hoa_hong_chua_lay += $item->sanPham->doanh_thu_con_lai;
                $hoa_hong_ve_cong_ty_da_lay += ($item->sanPham->doanh_thu_da_lay - $item->sanPham->da_thanh_toan_nhap_hang - $item->sanPham->da_thanh_toan_ban_hang - $item->sanPham->da_lay_hoa_hong_chuyen_khach);
            }
        }
        return [
            'doanh_thu_hoa_hong' => $doanh_thu_hoa_hong,
            'hoa_hong_ve_cong_ty' => $hoa_hong_ve_cong_ty,
            'hoa_hong_chua_lay' => $hoa_hong_chua_lay,
            'hoa_hong_ve_cong_ty_chua_lay' => $hoa_hong_ve_cong_ty - $hoa_hong_ve_cong_ty_da_lay,
        ];
    }

    public function getHoaHongVe($month, $year){
        $hoa_hong_da_nhan = 0;
        $hoa_hong_ve_cong_ty_da_nhan = 0;

        $lich_su_tien_ves = LichSuTienVe::find()
            ->andFilterWhere(['user_id' => $this->id])
            ->andFilterWhere(['thang' => $month])
            ->andFilterWhere(['nam' => $year])
            ->all();

        if(!empty($san_phams)) {
            foreach ($lich_su_tien_ves as $lich_su_tien_ve) {
                /** @var $lich_su_tien_ve LichSuTienVe */
                if($lich_su_tien_ve->type == LichSuTienVe::HOA_HONG) {
                    $hoa_hong_da_nhan += $lich_su_tien_ve->so_tien;
                }
                else{
                    $hoa_hong_ve_cong_ty_da_nhan += $lich_su_tien_ve->so_tien;
                }
            }
        }
        return [
            'hoa_hong_da_nhan' => $hoa_hong_da_nhan,
            'hoa_hong_ve_cong_ty_da_nhan' => $hoa_hong_ve_cong_ty_da_nhan,
        ];
    }
}
