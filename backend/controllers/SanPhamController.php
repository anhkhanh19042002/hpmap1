<?php

namespace backend\controllers;

use app\models\TrangThaiBanHang;
use backend\models\AnhSanPham;
use backend\models\CauHinh;
use backend\models\DanhMuc;
use backend\models\GiaoDich;
use backend\models\LichSuDoanhThu;
use backend\models\QuanLyUser;
use backend\models\search\QuanLySanPhamSearch;
use backend\models\ThongTinBanHang;
use backend\models\ThuongPhat;
use backend\models\TrangThaiSanPham;
use common\models\exportDanhSachSanPham;
use common\models\exportDanhSachSanPhamDaBan;
use common\models\exportDanhSachSanPhamDaDuyet;
use common\models\exportDanhSachSanPhamDangBan;
use common\models\User;
use common\models\myAPI;
use Yii;
use backend\models\SanPham;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use function Complex\add;
use common\models\VietHungAPI;

class SanPhamController extends CoreController
{
    public function behaviors()
    {
        $arr_action = ['index', 'delete','save-search','index-search','xem-chi-tiet', 'xac-nhan-san-pham', 'xem-chi-tiet-san-pham', 'san-pham-dang-cho-duyet', 'xoa',
            'san-pham-phu-trach', 'luu-thong-tin-ban-hang', 'san-pham-da-ban', 'luu-nhan-vien-phu-trach', 'luu-khach-hang-tiem-nang',
            'view-khach-hang-tiem-nang', 'save', 'xac-nhan-phu-trach', 'thong-ke', 'thong-ke-san-pham', 'tai-danh-sach-san-pham-dang-ban',
            'tai-danh-sach-san-pham-da-ban', 'xac-nhan-ban-lai', 'huy-san-pham', 'tai-danh-sach-kho-san-pham', 'update-nv-cap-nhat-phap-ly',
            'tai-danh-sach-kho-san-pham', 'tai-danh-sach-kho-san-pham-dang-ban', 'tai-danh-sach-kho-san-pham-da-duyet',
            'da-ban-san-pham', 'xoa-khach-hang', 'danh-sach-san-pham-phu-trach', 'update-nhu-cau-khach-hang',
            'danh-sach-san-pham-trong-kho', 'luu-thong-tin-ban', 'xoa-anh', 'xoa-thong-tin-ban', 'luu-thong-tin-ban-view', 'cap-nhat-doanh-thu',
            'xoa-lich-su-doanh-thu', 'search', 'search-da-ban', 'search-phu-trach', 'search-cho-duyet', 'xoa-nhieu-san-pham',
            'san-pham-het-han', 'luu-gia-han', 'cho-phep-duyet-san-pham','luu-nhan-vien-phu-trach-san-pham'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('SanPham', $action_name);
                }
            ];
        }

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' =>$rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** index */
    public function actionSaveSearch()
    {
        $searchModel = new QuanLySanPhamSearch();
        if (isset($_POST['search']))
        {
            $request = Yii::$app->request;
                $search['QuanLySanPhamSearch']['id'] =$_POST['search'];
                $search['r'] = 'category/quan-ly-san-pham';
                $search['_pjax'] = '#crud-datatable-pjax';
                Yii::$app->session->set('searchQuanLySanPham', $search);

        }
        if (isset($_POST['value']))
        {
            $request = Yii::$app->request;
            $search['QuanLySanPhamSearch']['sap_xep_gia'] =$_POST['value'];
            $search['r'] = 'category/quan-ly-san-pham';
            $search['_pjax'] = '#crud-datatable-pjax';
            Yii::$app->session->set('searchQuanLySanPham', $search);

        }
        Yii::$app->response->format = Response::FORMAT_JSON;
       return Yii::$app->session['searchQuanLySanPham'];
    }
    public function actionIndex()
    {

        if(isset(Yii::$app->session['searchQuanLySanPham'])){
            Yii::$app->request->setQueryParams(Yii::$app->session['searchQuanLySanPham']);
            unset(Yii::$app->session['searchQuanLySanPham']);
        }
        $searchModel = new QuanLySanPhamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(isset($_GET['QuanLySanPhamSearch']))
            Yii::$app->session->set('params_SanPham', $_GET['QuanLySanPhamSearch']);
        else{
            if(Yii::$app->session->get('params_SanPham'))
                $searchModel->attributes = Yii::$app->session->get('params_SanPham');
        }
        $view ='index';
        if(!User::isUpdateAll())
            $view = 'index-nhan-vien';
        return $this->render($view, [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            'search'=>Yii::$app->session,
            ]);
    }
    /** view */
    public function actionXemChiTiet()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://hpmap.vn/convert-url',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('nid' => $_POST['value']),
            CURLOPT_HTTPHEADER => array(
                'Cookie: SSESSebf3c8dd14349cc7bdfae069b63688f0=H9e2Le6vBoQirMIpYeFlTWZVpswdYSffYo0QZCv6DNs'
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return
            [
                'content'=>"https://hpmap.vn/".\GuzzleHttp\json_decode($response)
            ];
    }
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "SanPham #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
    /** delete */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }

    }

     /** bulkdelete */
    public function actionBulkdelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' ));
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            return $this->redirect(['index']);
        }

    }

    /** save */
    public function actionSave(){
        if($_POST['SanPham']['id'] == '')
            $sanPham = new SanPham();
        else
            $sanPham = SanPham::findOne($_POST['SanPham']['id']);
        $sanPham->load(Yii::$app->request->post());

        if($sanPham->save()){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'content' => '<p>Đã lưu sản phẩm thành công.</p>'
                    .Html::a('<i class="fa fa-plus-square-o"></i> Thêm sản phẩm', Url::toRoute(['san-pham/create']), ['class' => 'btn btn-primary btn-sm'])
                    .Html::a('<i class="fa fa-arrow-circle-left"></i> Quay lại danh sách', Url::toRoute(['san-pham/san-pham-dang-cho-duyet']), ['class' => 'btn btn-success btn-sm']).
                    '</p>',
                'title' => 'Thông báo'
            ];
       }
       else
            throw new HttpException(500, \yii\bootstrap\Html::errorSummary($sanPham));

    }

    /** xac-nhan-san-pham */
    public function actionXacNhanSanPham(){
        $vietHungAPI = new VietHungAPI();
        $slSPChoDuyet = $vietHungAPI->getSoLuongSPChoDuyet();
        $slSPPhuTrach = $vietHungAPI->getSoLuongSPDangPhuTrach();
        if (!is_null($_POST['san_pham_id'])){
            $trang_thai = new TrangThaiSanPham();
            $trang_thai->san_pham_id = $_POST['san_pham_id'];
            $trang_thai->trang_thai = $_POST['SanPham']['trang_thai'];
            $trang_thai->ghi_chu = $_POST['SanPham']['ghi_chu'];
            if ($trang_thai->save()) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                if ($trang_thai->trang_thai === SanPham::DANG_BAN) {
                    return [
                        'soluong'=>$slSPChoDuyet + $slSPPhuTrach,
                        'content' => 'Đã duyệt sản phẩm thành công',
                        'title' => 'Thông báo',
                        'nid' => $trang_thai->sanPham->nid,
                    ];
                } else {
                    return [
                        'content' => 'Đã duyệt sản phẩm thành công',
                        'title' => 'Thông báo'
                    ];
                }
            } else {
                throw new HttpException(500, Html::errorSummary($trang_thai));
            }
        } else {
            throw new HttpException(500, 'Không xác thực được dữ liệu');
        }
    }

    /** xem-chi-tiet-san-pham */
    public function actionXemChiTietSanPham(){
        if (!is_null($_POST['id'])) {
            $time = isset($_POST['time']) ? $_POST['time'] : '';
            $model = SanPham::findOne($_POST['id']);
            $tinhTrang = $model->trangThaiSanPhams;
            $thongTinBanHang = ThongTinBanHang::find()->andFilterWhere(['active' => 1, 'san_pham_id' => $_POST['id']])
                ->andWhere('khach_hang_id is not null')->orderBy(['ngay_xem'=>SORT_DESC])
                ->all();

            $arr = array_filter(explode('<br />', nl2br($model->link_video)));
            $links = [];
            foreach ($arr as $index => $item){
                $links[trim($item)] = trim($item);
            }
            $chiTietBan = ThongTinBanHang::find()->andFilterWhere(['trang_thai'=>TrangThaiBanHang::DA_MUA,'active' => 1, 'san_pham_id' => $_POST['id']])
                ->andWhere('khach_hang_id is null')->all();
            $lsDoanhThu = LichSuDoanhThu::find()->andFilterWhere(['san_pham_id' => $_POST['id']])->all();

            Yii::$app->response->format = Response::FORMAT_JSON;
            return[
                'content' => $this->renderAjax('xem-chi-tiet-san-pham', [
                    'model' => $model,
                    'tinhTrang' => $tinhTrang,
                    'thongTinBanHang' => $thongTinBanHang,
                    'links' => $links,
                    'chiTietBan' => $chiTietBan,
                    'lsDoanhThu' => $lsDoanhThu,
                    'time' => $time
                ]),
                'title' => 'Xem chi tiết sản phẩm '
            ];
        } else {
            throw new HttpException(500, 'Không xác thực được dữ liệu');
        }
    }

    /** san-pham-dang-cho-duyet */
    public function actionSanPhamDangChoDuyet()
    {
        if(isset(Yii::$app->session['searchQuanLySanPhamChoDuyet'])){
            Yii::$app->request->setQueryParams(Yii::$app->session['searchQuanLySanPhamChoDuyet']);
            unset(Yii::$app->session['searchQuanLySanPhamChoDuyet']);
        }

        $searchModel = new QuanLySanPhamSearch();
        $dataProvider = $searchModel->searchChoDuyet(Yii::$app->request->queryParams);

        return $this->render('san-pham-cho-duyet/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /** xoa */
    public function actionXoa(){
        $san_pham = SanPham::findOne($_POST['id']);
        $user = QuanLyUser::find()->andFilterWhere(['id'=>Yii::$app->user->id])->andWhere("vai_tro like '%Trưởng phòng%'")->one();
        if($san_pham->trang_thai === SanPham::CHO_DUYET || User::isViewAll() == true || $user != null) {
            $san_pham->updateAttributes(['active' => 0]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'id'=>$san_pham->nid,
                'content' => 'Đã xóa sản phẩm thành công',
                'title' => 'Thông báo'
            ];
        }else
            throw new HttpException(500,'Sản phẩm đang trong trạng thái không thể xoá');
    }

    /** san-pham-phu-trach */
    public function actionSanPhamPhuTrach()
    {
        if(isset(Yii::$app->session['searchQuanLySanPhamPhuTrach'])){
            Yii::$app->request->setQueryParams(Yii::$app->session['searchQuanLySanPhamPhuTrach']);
            unset(Yii::$app->session['searchQuanLySanPhamPhuTrach']);
        }

        $searchModel = new QuanLySanPhamSearch();
        $dataProvider = $searchModel->searchPhuTrach(Yii::$app->request->queryParams);

        if(isset($_GET['QuanLySanPhamSearch']))
            Yii::$app->session->set('params_SanPham', $_GET['QuanLySanPhamSearch']);
        else{
            if(Yii::$app->session->get('params_SanPham'))
                $searchModel->attributes = Yii::$app->session->get('params_SanPham');
        }

        $view = 'san-pham-phu-trach/index';
        if(!User::isViewAll())
            $view = 'san-pham-phu-trach/index-nhan-vien';
        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /** luu-thong-tin-ban-hang */
    public function actionLuuThongTinBanHang(){
        if (!is_null($_POST['san_pham_id'])){
                $trang_thai = $_POST['ThongTinBanHang']['trang_thai'];
                $thong_tin_ban = new ThongTinBanHang();
                $thong_tin_ban->load(Yii::$app->request->post());
                $thong_tin_ban->san_pham_id = $_POST['san_pham_id'];
                $thong_tin_ban->trang_thai = TrangThaiBanHang::DA_MUA;
                if ($thong_tin_ban->kieu_nguoi_ban == ThongTinBanHang::NHAN_VIEN){
                    $thong_tin_ban->nguoi_ban_id = $_POST['ThongTinBanHang']['nguoi_ban_id'];
                    $thong_tin_ban->nguoi_ban = $thong_tin_ban->nguoiBan->hoten;
                }elseif ($thong_tin_ban->kieu_nguoi_ban == ThongTinBanHang::CHU_NHA){
                    $thong_tin_ban->nguoi_ban_id = '';
                    $thong_tin_ban->nguoi_ban = SanPham::findOne([$_POST['san_pham_id']])->chu_nha;
                }elseif ($thong_tin_ban->kieu_nguoi_ban == SanPham::DON_VI_KHAC_BAN){
                    $thong_tin_ban->nguoi_ban_id = '';
                    $thong_tin_ban->nguoi_ban = $_POST['ThongTinBanHang']['nguoi_ban'];
                }
                if ($thong_tin_ban->save()){
                    $ngay_ban = myAPI::convertDMY2YMD($_POST['ThongTinBanHang']['ngay_ban']);
                    if ($thong_tin_ban->nguoi_ban_id == ''){
                        $thong_tin_ban->sanPham->updateAttributes(
                            [
                                'nguoi_ban'=>$thong_tin_ban->nguoi_ban,
                                'nhan_vien_ban_id'=>$thong_tin_ban->nguoi_ban_id,
                                'trang_thai'=>SanPham::DA_MAT,
                                'type_nguoi_ban' => $_POST['ThongTinBanHang']['kieu_nguoi_ban'],
                                'ghi_chu' => $thong_tin_ban->sanPham->ghi_chu,
                                'ngay_ban' => $ngay_ban,
                            ]
                        );
                    }else{
                        $thong_tin_ban->sanPham->updateAttributes(
                            [
                                'nguoi_ban'=>$thong_tin_ban->nguoi_ban,
                                'nhan_vien_ban_id'=>$thong_tin_ban->nguoi_ban_id,
                                'trang_thai'=>$trang_thai,
                                'type_nguoi_ban' => $_POST['ThongTinBanHang']['kieu_nguoi_ban'],
                                'ghi_chu' => $thong_tin_ban->sanPham->ghi_chu,
                                'ngay_ban' => $ngay_ban,
                            ]
                        );
                    }
                    $lsDoanhThu = LichSuDoanhThu::findOne(['san_pham_id' => $thong_tin_ban->sanPham->id,
                        'active' => LichSuDoanhThu::ACTIVE]);
                    if(!is_null($lsDoanhThu)){
                        $lsDoanhThu->updateAttributes(['ngay_ban' => $ngay_ban, 'updated' => date('Y-m-d H:i:s'),
                            'nguoi_ban_id'=>$thong_tin_ban->nguoi_ban_id]);
                    }

                    $trangThaiSanPham = new TrangThaiSanPham();
                    $trangThaiSanPham->san_pham_id = $_POST['san_pham_id'];
                    $trangThaiSanPham->trang_thai = $trang_thai;
                    $trangThaiSanPham->save();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return[
                        'content' => 'Đã lưu thông tin bán hàng thành công',
                        'title' => 'Thông báo'
                    ];
                }else
                    throw new HttpException(500, Html::errorSummary($thong_tin_ban));
            }else{
            throw new HttpException(500, 'Không có thông tin sản phẩm');
        }
    }

    /** san-pham-da-ban */
    public function actionSanPhamDaBan()
    {
        if(isset(Yii::$app->session['searchQuanLySanPhamDaBan'])){
            Yii::$app->request->setQueryParams(Yii::$app->session['searchQuanLySanPhamDaBan']);
            unset(Yii::$app->session['searchQuanLySanPhamDaBan']);
        }

        $searchModel = new QuanLySanPhamSearch();
        $dataProvider = $searchModel->searchDaBan(Yii::$app->request->queryParams);

        if(isset($_GET['QuanLySanPhamSearch']))
            Yii::$app->session->set('params_SanPham', $_GET['QuanLySanPhamSearch']);
        else{
            if(Yii::$app->session->get('params_SanPham'))
                $searchModel->attributes = Yii::$app->session->get('params_SanPham');
        }

        return $this->render('san-pham-da-ban/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    //luu-nhan-vien-phu-trach
    public function actionLuuNhanVienPhuTrach(){
        foreach ($_POST['SanPham'] as $idSP) {
            $trangThaiSP = new TrangThaiSanPham();
            $trangThaiSP->san_pham_id = $idSP;
            $trangThaiSP->trang_thai = SanPham::DANG_BAN;
            $trangThaiSP->save();
        }
        SanPham::updateAll(['nhan_vien_phu_trach_id' => $_POST['nhan_vien']], ['in', 'id', $_POST['SanPham']]);
        $user_phu_trach=User::findOne(['id'=>$_POST['nhan_vien']]);
        myAPI::sendMail(
            'Bạn có sản phẩm được phụ trách vui lòng truy cập vào đường link: https://hpmap.vn/ để biết thêm thông tin chi tiết.',
            ['hungdd@minhhien.com.vn'=>'Hpmap'],
            $user_phu_trach->email,
            'Sản phẩm phụ trách'
        );
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Đã cập nhật nhân viên phụ trách thành công',
            'title' => 'Thông báo'
        ];
    }
    //luu-nhan-vien-phu-trach-san-pham
    public function actionLuuNhanVienPhuTrachSanPham(){
        $trangThaiSP = new TrangThaiSanPham();
        $trangThaiSP->san_pham_id = $_POST['sanPham'];
        $trangThaiSP->trang_thai = SanPham::DANG_BAN;
        $trangThaiSP->save();
        SanPham::updateAll(['nhan_vien_phu_trach_id'=>$_POST['nhan_vien']],['id'=>$_POST['sanPham']]);
        $user_phu_trach=User::findOne(['id'=>$_POST['nhan_vien']]);
        myAPI::sendMail(
            'Bạn có sản phẩm được phụ trách vui lòng truy cập vào đường link: https://hpmap.vn/ để biết thêm thông tin chi tiết.',
            ['hungdd@minhhien.com.vn'=>'Hpmap'],
            $user_phu_trach->email,
            'Sản phẩm phụ trách'
        );
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [

            'content' => 'Đã cập nhật nhân viên phụ trách thành công',
            'title' => 'Thông báo'
        ];
    }
    //luu-khach-hang-tiem-nang
    public function actionLuuKhachHangTiemNang(){

        $message = '';
        $loi = [];

        if(isset($_POST['User'])){
            $model = new User();
            $model->load(Yii::$app->request->post());
            if($model->save()){
                $thongTinBanHang = new ThongTinBanHang();
                $thongTinBanHang->san_pham_id = $_POST['san_pham'];
                $thongTinBanHang->khach_hang_id = $model->id;
                $thongTinBanHang->ghi_chu = $model->ghi_chu;
                $thongTinBanHang->ngay_xem = $_POST['ThongTinBanHang']['ngay_xem'];
                if($thongTinBanHang->save())
                    $message = 'Đã lưu thành công thông tin khách hàng';
                else
                    $loi[] = Html::errorSummary($thongTinBanHang);
            }else
                $loi[] = Html::errorSummary($model);
        }
        else{
            $soLuong = 0;
            if(isset($_POST['khach_hang']))
                foreach ($_POST['khach_hang'] as $idKhach => $value) {
                    $thongTinBanHang = new ThongTinBanHang();
                    $thongTinBanHang->san_pham_id = $_POST['san_pham'];
                    $thongTinBanHang->khach_hang_id = $idKhach;
                    $thongTinBanHang->ngay_xem = $_POST['ngay_xem'][$idKhach];
                    if(!$thongTinBanHang->save())
                        $loi[] = Html::errorSummary($thongTinBanHang);
                    else
                        $soLuong++;
                }
            $message = 'Đã lưu '.$soLuong.' khách hàng đã xem thành công';
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => '<p>'.$message.'</p>'.(count($loi) > 0 ? myAPI::getMessage('danger', implode('<br/>', $loi)) : ""),
            'title' => 'Thông báo'
        ];
    }

    //view-khach-hang-tiem-nang
    public function actionViewKhachHangTiemNang(){
        $thongTinBanHang = ThongTinBanHang::find()->andFilterWhere(['active' => 1, 'san_pham_id' => $_POST['san_pham']])
            ->andWhere('khach_hang_id is not null')->orderBy(['ngay_xem'=>SORT_DESC])
            ->all();
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => $this->renderAjax('san-pham-phu-trach/_ds_khach_hang_tiem_nang',[
                'data' => $thongTinBanHang
            ]),
            'title'=> 'Khách hàng đã xem'
        ];
    }

    //xac-nhan-phu-trach
    public function actionXacNhanPhuTrach(){
        $model = new TrangThaiSanPham();
        $model->load(Yii::$app->request->post());
        if($model->save()){
            if($model->trang_thai == SanPham::DA_DUYET){
                $model->sanPham->updateAttributes(['nhan_vien_phu_trach_id' => null]);
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->trang_thai === SanPham::DANG_BAN) {
                return [
                    "content" => 'Đã cập nhật trạng thái thành công',
                    'title' => 'Thông báo',
                    'nid' => $model->sanPham->nid,
                ];
            }else{
                return [
                    "content" => 'Đã cập nhật trạng thái thành công',
                    'title' => 'Thông báo',
                ];
            }
        }else
            throw new HttpException(500, Html::errorSummary($model));
    }

    /** thong-ke */
    public function actionThongKe(){
        $listUser = ArrayHelper::map(User::find()->andFilterWhere(['nhom'=>User::THANH_VIEN,'active'=>1,'status'=>10])->all(),'id','hoten');
        if(!isset($_POST['loai_thong_ke']))
            return $this->render('thong-ke',[
                'listUser' => $listUser
            ]);
        else{
            $dataThongKe = [];
            $thoi_gian = [];
            $date = [];
            $value = [];
            if($_POST['loai_thong_ke'] == 'Theo tháng'){
                $tu_thang = $_POST['tu_thang'] + 1;
                $tu_nam = $_POST['tu_nam'];
                $tu_thang = sprintf('%02d', $tu_thang);

                while ($tu_nam.'-'.$tu_thang <= $_POST['den_nam'].'-'.sprintf('%02d', $_POST['den_thang']+ 1)){
                    $thoi_gian[$tu_nam.'-'.$tu_thang] = $tu_thang.'/'.$tu_nam;
                    $currentMonth = mktime(0,0,0, $tu_thang,1,$tu_nam);
                    $nextMonth = strtotime("+1 month", $currentMonth);
                    $tu_thang = date("m", $nextMonth);
                    $tu_nam = date("Y", $nextMonth);
                }

                if ($_POST['nhan_vien_thong_ke'] == ''){
                    foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                        $arr_thang_nam = explode('-', $index_thoi_gian);
                        $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_khach_hang_theo_thang(:thang, :nam)', [
                            ':thang' => $arr_thang_nam[1],
                            ':nam' => $arr_thang_nam[0]
                        ])->queryAll();
//                    $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_khach_hang']) ? $ketQua[0]['so_khach_hang'] : 0;
                        $date[] = $index_thoi_gian;
                        $value[] = isset($ketQua[0]['so_khach_hang']) ? $ketQua[0]['so_khach_hang'] : 0;
                    }
                }else{
                    foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                        $arr_thang_nam = explode('-', $index_thoi_gian);
                        $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_khach_hang_theo_thang_do_nhan_vien(:thang, :nam, :nhanvien)', [
                            ':thang' => $arr_thang_nam[1],
                            ':nam' => $arr_thang_nam[0],
                            ':nhanvien' => $_POST['nhan_vien_thong_ke']
                        ])->queryAll();
                        $date[] = $index_thoi_gian;
                        $value[] = isset($ketQua[0]['so_khach_hang']) ? $ketQua[0]['so_khach_hang'] : 0;
                    }
                }

            }
            else{
//                $content = $this->getThongKeNgay();
                $thoiGianTu = myAPI::convertDateSaveIntoDb($_POST['thoigianthongke_tu']);
                $thoiGianDen = myAPI::convertDateSaveIntoDb($_POST['thoigianthongke_den']);

                while ($thoiGianTu <= $thoiGianDen){
                    $thoi_gian[$thoiGianTu] = date("d/m/Y", strtotime($thoiGianTu));
                    $thoiGianTu = date("Y-m-d", strtotime("+1 day", strtotime($thoiGianTu)));
                }
                if ($_POST['nhan_vien_thong_ke'] == ''){
                    foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                        $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_khach_hang_theo_ngay(:ngay)', [
                            ':ngay' => $index_thoi_gian,
                        ])->queryAll();
                        $date[] = $index_thoi_gian;
                        $value[] = isset($ketQua[0]['so_khach_hang']) ? $ketQua[0]['so_khach_hang'] : 0;
                    }
                }else{
                    foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                        $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_khach_hang_theo_ngay_do_nhan_vien(:ngay, :nhanvien)', [
                            ':ngay' => $index_thoi_gian,
                            ':nhanvien' => $_POST['nhan_vien_thong_ke']
                        ])->queryAll();
                        $date[] = $index_thoi_gian;
                        $value[] = isset($ketQua[0]['so_khach_hang']) ? $ketQua[0]['so_khach_hang'] : 0;
                    }
                }

            }


            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
//                'content' => $content,
//                'dataThongKe' => $dataThongKe,
                'thoi_gian' => $thoi_gian,
                'tu_ngay' => $_POST['thoigianthongke_tu'],
                'den_ngay' => $_POST['thoigianthongke_den'],
                'date' => $date,
                'value' => $value
            ];

        }
    }

    /** thong-ke-san-pham */
    public function actionThongKeSanPham(){
        $listUser = ArrayHelper::map(User::find()->andFilterWhere(['nhom'=>User::THANH_VIEN,'active'=>1,'status'=>10])->all(),'id','hoten');
        if(!isset($_POST['loai_thong_ke']))
            return $this->render('thong-ke-san-pham',[
                'listUser' => $listUser
            ]);
        else{
//            $dataThongKe = [];
            $thoi_gian = [];
            $date = [];
            $value = [];
            if($_POST['loai_thong_ke'] == 'Theo tháng'){
//                $content = $this->getThongKeSanPhamThang();
                $tu_thang = $_POST['tu_thang'] + 1;
                $tu_nam = $_POST['tu_nam'];
                $tu_thang = sprintf('%02d', $tu_thang);
                $kieu_thong_ke = $_POST['kieu_thong_ke'];

                while ($tu_nam.'-'.$tu_thang <= $_POST['den_nam'].'-'.sprintf('%02d', $_POST['den_thang']+ 1)){
                    $thoi_gian[$tu_nam.'-'.$tu_thang] = $tu_thang.'/'.$tu_nam;
                    $currentMonth = mktime(0,0,0, $tu_thang,1,$tu_nam);
                    $nextMonth = strtotime("+1 month", $currentMonth);
                    $tu_thang = date("m", $nextMonth);
                    $tu_nam = date("Y", $nextMonth);
                }

                if ($kieu_thong_ke == 'Đã bán'){
                    if ($_POST['nhan_vien_thong_ke'] == ''){
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $arr_thang_nam = explode('-', $index_thoi_gian);
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_tat_ca_san_pham_da_ban_theo_thang(:thang, :nam)', [
                                ':thang' => $arr_thang_nam[1],
                                ':nam' => $arr_thang_nam[0]
                            ])->queryAll();
//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }else{
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $arr_thang_nam = explode('-', $index_thoi_gian);
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_san_pham_da_ban_do_nhan_vien_theo_thang(:thang, :nam, :nhan_vien)', [
                                ':thang' => $arr_thang_nam[1],
                                ':nam' => $arr_thang_nam[0],
                                ':nhan_vien' => $_POST['nhan_vien_thong_ke']
                            ])->queryAll();
//                            VarDumper::dump($ketQua, 10, true); exit();
//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }
                }elseif ($kieu_thong_ke == 'Đang bán'){
                    if ($_POST['nhan_vien_thong_ke'] == ''){
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $arr_thang_nam = explode('-', $index_thoi_gian);
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_tat_ca_san_pham_dang_ban_theo_thang(:thang, :nam)', [
                                ':thang' => $arr_thang_nam[1],
                                ':nam' => $arr_thang_nam[0]
                            ])->queryAll();
//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }else{
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $arr_thang_nam = explode('-', $index_thoi_gian);
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_san_pham_dang_ban_do_nhan_vien_theo_thang(:thang, :nam, :nhan_vien)', [
                                ':thang' => $arr_thang_nam[1],
                                ':nam' => $arr_thang_nam[0],
                                ':nhan_vien' => $_POST['nhan_vien_thong_ke']
                            ])->queryAll();
//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }
                }
            }
            else{
//                $content = $this->getThongKeSanPhamNgay();
                $thoiGianTu = myAPI::convertDateSaveIntoDb($_POST['thoigianthongke_tu']);
                $thoiGianDen = myAPI::convertDateSaveIntoDb($_POST['thoigianthongke_den']);
                $kieu_thong_ke = $_POST['kieu_thong_ke'];

                while ($thoiGianTu <= $thoiGianDen){
                    $thoi_gian[$thoiGianTu] = date("d/m/Y", strtotime($thoiGianTu));
                    $thoiGianTu = date("Y-m-d", strtotime("+1 day", strtotime($thoiGianTu)));
                }
                if ($kieu_thong_ke == 'Đã bán'){
                    if ($_POST['nhan_vien_thong_ke'] == ''){
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_tat_ca_san_pham_da_ban_theo_ngay (:ngay)', [
                                ':ngay' => $index_thoi_gian,
                            ])->queryAll();

//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }else{
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_san_pham_da_ban_do_nhan_vien_theo_ngay(:ngay, :nhan_vien)', [
                                ':ngay' => $index_thoi_gian,
                                ':nhan_vien' => $_POST['nhan_vien_thong_ke']
                            ])->queryAll();

//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }
                }elseif ($kieu_thong_ke == 'Đang bán'){
                    if ($_POST['nhan_vien_thong_ke'] == ''){
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_tat_ca_san_pham_dang_ban_theo_ngay (:ngay)', [
                                ':ngay' => $index_thoi_gian,
                            ])->queryAll();

//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }else{
                        foreach ($thoi_gian as $index_thoi_gian => $value_thoi_gian) {
                            $ketQua = Yii::$app->db->createCommand('CALL vh_thong_ke_san_pham_dang_ban_do_nhan_vien_theo_ngay(:ngay, :nhan_vien)', [
                                ':ngay' => $index_thoi_gian,
                                ':nhan_vien' => $_POST['nhan_vien_thong_ke']
                            ])->queryAll();

//                            $dataThongKe[$index_thoi_gian] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                            $date[] = $index_thoi_gian;
                            $value[] = isset($ketQua[0]['so_san_pham']) ? $ketQua[0]['so_san_pham'] : 0;
                        }
                    }
                }
            }

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
//                'content' => $content
//                'dataThongKe' => $dataThongKe,
                'thoi_gian' => $thoi_gian,
                'tu_thang' => sprintf('%02d', ($_POST['tu_thang']) + 1).'/'.$_POST['tu_nam'],
                'den_thang' => sprintf('%02d',( $_POST['den_thang'])+1).'/'.$_POST['den_nam'],
                'date'=>$date,
                'value'=> $value
            ];

        }
    }

    /** tai-danh-sach-san-pham-dang-ban */
    public function actionTaiDanhSachSanPhamDangBan(){
        if(Yii::$app->session->get('params_SanPham')){
            $data = (new QuanLySanPhamSearch())->searchPhuTrach(['QuanLySanPhamSearch' => Yii::$app->session->get('params_SanPham')]);
        }
        else
            $data = (new QuanLySanPhamSearch())->searchPhuTrach(Yii::$app->request->queryParams);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $data->pagination = false;
        $data = $data->getModels();
        $export = new exportDanhSachSanPhamDangBan();
        $export->data = $data;
        $export->stream = false;
        $export->path_file = dirname(dirname(__DIR__)).'/files_excel/';
        $file_name = $export->run();
        $file =  str_replace('index.php','', Url::home(true)).'files_excel/'.$file_name;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => 'Tải file kết quả',
            'link_file' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để tải file về!', $file, ['class' => 'text-primary', 'target' => '_blank'])
        ];

    }

    /** tai-danh-sach-san-pham-da-ban */
    public function actionTaiDanhSachSanPhamDaBan(){
        if(Yii::$app->session->get('params_SanPham')){
            $data = (new QuanLySanPhamSearch())->searchDaBan(['QuanLySanPhamSearch' => Yii::$app->session->get('params_SanPham')]);
        }
        else
            $data = (new QuanLySanPhamSearch())->searchDaBan(Yii::$app->request->queryParams);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $data->pagination = false;
        $data = $data->getModels();

        $export = new exportDanhSachSanPhamDaBan();
        $export->data = $data;
        $export->stream = false;
        $export->path_file = dirname(dirname(__DIR__)).'/files_excel/';
        $file_name = $export->run();
        $file =  str_replace('index.php','', Url::home(true)).'files_excel/'.$file_name;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => 'Tải file kết quả',
            'link_file' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để tải file về!', $file, ['class' => 'text-primary', 'target' => '_blank'])
        ];
    }

    /** xac-nhan-ban-lai */
    public function actionXacNhanBanLai(){
        if (!is_null($_POST['san_pham_id'])){
            $trang_thai = new TrangThaiSanPham();
            $trang_thai->san_pham_id = $_POST['san_pham_id'];
            $trang_thai->trang_thai = $_POST['SanPham']['trang_thai'];
            $trang_thai->ghi_chu = $_POST['SanPham']['ghi_chu'];
            if ($trang_thai->save()){
                $trang_thai->sanPham->updateAttributes(['nhan_vien_ban_id'=>'','nhan_vien_phu_trach_id'=>'','ngay_ban'=>null]);
                foreach ($trang_thai->sanPham->lichSuDoanhThus as $item){
                    /** @var $item LichSuDoanhThu */
                    $item->updateAttributes(['active' => LichSuDoanhThu::IN_ACTIVE, 'updated' => date('Y-m-d H:i:s')]);
                }
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => 'Đã xác nhận sản phẩm thành công. Vui lòng vào kho sản phẩm để kiểm tra.',
                    'title' => 'Thông báo',
                    'arr' => [$_POST['san_pham_id']],
                ];
            } else {
                throw new HttpException(500, Html::errorSummary($trang_thai));
            }
        } else {
            throw new HttpException(500, 'Không xác thực được dữ liệu');
        }
    }

    /** huy-san-pham */
    public function actionHuySanPham(){
        $user = User::findOne(Yii::$app->user->id);
        $san_pham = SanPham::findOne($_POST['id']);
        $trang_thai = new TrangThaiSanPham();
        $trang_thai->san_pham_id = $_POST['id'];
        $trang_thai->trang_thai = SanPham::DA_DUYET;
        $trang_thai->ghi_chu = 'Sản phẩm đã bị hủy'.(is_null($user) == true ? '.' : ' bởi '.$user->hoten);
        if ($trang_thai->save()){
            $san_pham->updateAttributes(['nhan_vien_phu_trach_id' => null]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return[
                'content' => 'Đã hủy sản phẩm thành công',
                'title' => 'Thông báo'
            ];
        }else
            throw new HttpException(500, Html::errorSummary($trang_thai));
    }

    /** tai-danh-sach-san-pham-dang-ban */
    public function actionTaiDanhSachKhoSanPhamDangBan(){
        if(Yii::$app->session->get('params_SanPham')){
            $data = (new QuanLySanPhamSearch())->searchPhuTrachTatCa(['QuanLySanPhamSearch' => Yii::$app->session->get('params_SanPham')]);
        }
        else
            $data = (new QuanLySanPhamSearch())->searchPhuTrachTatCa(Yii::$app->request->queryParams);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $data->pagination = false;
        $data = $data->getModels();
        $export = new exportDanhSachSanPhamDangBan();
        $export->data = $data;
        $export->stream = false;
        $export->path_file = dirname(dirname(__DIR__)).'/files_excel/';
        $file_name = $export->run();
        $file =  str_replace('index.php','', Url::home(true)).'files_excel/'.$file_name;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => 'Tải file kết quả',
            'link_file' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để tải file về!', $file, ['class' => 'text-primary', 'target' => '_blank'])
        ];

    }

    /** tai-danh-sach-san-pham-da-duyet */
    public function actionTaiDanhSachKhoSanPhamDaDuyet(){
        if(Yii::$app->session->get('params_SanPham')){
            $data = (new QuanLySanPhamSearch())->searchDaDuyetTatCa(['QuanLySanPhamSearch' => Yii::$app->session->get('params_SanPham')]);
        }
        else
            $data = (new QuanLySanPhamSearch())->searchDaDuyetTatCa(Yii::$app->request->queryParams);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $data->pagination = false;
        $data = $data->getModels();

        $export = new exportDanhSachSanPhamDaDuyet();
        $export->data = $data;
        $export->stream = false;
        $export->path_file = dirname(dirname(__DIR__)).'/files_excel/';
        $file_name = $export->run();
        $file =  str_replace('index.php','', Url::home(true)).'files_excel/'.$file_name;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => 'Tải file kết quả',
            'link_file' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để tải file về!', $file, ['class' => 'text-primary', 'target' => '_blank'])
        ];
    }

    /** tai-danh-sach-kho-san-pham */
    public function actionTaiDanhSachKhoSanPham(){
        if(Yii::$app->session->get('params_SanPham')){
            $data = (new QuanLySanPhamSearch())->search(['QuanLySanPhamSearch' => Yii::$app->session->get('params_SanPham')]);
        }
        else
            $data = (new QuanLySanPhamSearch())->search(Yii::$app->request->queryParams);
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');
        $data->pagination = false;
        $data = $data->getModels();

        $export = new exportDanhSachSanPham();
        $export->data = $data;
        $export->stream = false;
        $export->path_file = dirname(dirname(__DIR__)).'/files_excel/';
        $file_name = $export->run();
        $file =  str_replace('index.php','', Url::home(true)).'files_excel/'.$file_name;

        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => 'Tải file kết quả',
            'link_file' => Html::a('<i class="fa fa-cloud-download"></i> Nhấn vào đây để tải file về!', $file, ['class' => 'text-primary', 'target' => '_blank'])
        ];
    }

    //update-nv-cap-nhat-phap-ly
    public function actionUpdateNvCapNhatPhapLy(){
        foreach ($_POST['NVCapNhat'] as $idSanPham => $idNhanVien) {
            $model = SanPham::findOne($idSanPham);
            $model->updateAttributes([
                'nguoi_tao_id' => $idNhanVien == '' ? null : $idNhanVien,
                'phap_ly' => $_POST['TTPhapLy'][$idSanPham]
            ]);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'content' => 'Đã cập nhật thành công '.count($_POST['NVCapNhat']).' sản phẩm thành công',
            'title' => 'Thông báo'
        ];
    }

    /** da-ban-san-pham */
    public function actionDaBanSanPham(){
        if (!is_null($_POST['san_pham_id'])){
            $trang_thai_sp =  new TrangThaiSanPham();
            $trang_thai_sp->san_pham_id = $_POST['san_pham_id'];
            $trang_thai_sp->trang_thai = SanPham::DA_BAN;
            $trang_thai_sp->ghi_chu = $_POST['SanPham']['ghi_chu'];
            if ($trang_thai_sp->save()){
                $trang_thai_sp->sanPham->updateAttributes(['ghi_chu' => $trang_thai_sp->ghi_chu]);
                Yii::$app->response->format = Response::FORMAT_JSON;
                return[
                    'content' => 'Đã cập nhật trạng thái sản phẩm thành công',
                    'title' => 'Thông báo'
                ];
            }else{
                throw new HttpException(500, Html::errorSummary($trang_thai_sp));
            }
        } else {
            throw new HttpException(500, 'Không xác thực được dữ liệu');
        }
    }

    /** xoa-khach-hang */
    public function actionXoaKhachHang(){
        $khach_hang = ThongTinBanHang::findOne($_POST['id']);
        if ($khach_hang->updateAttributes(['active'=> 0,'trang_thai'=>TrangThaiBanHang::HUY])){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'content'=>'Đã xoá khách hàng '.$khach_hang->khachHang->hoten.'ra khỏi danh sách khách hàng tiềm năng thành công',
                'title'=>'Thông báo'
            ];
        }
        else{
            throw new HttpException(500,Html::errorSummary($khach_hang));
        }
    }

    /** danh-sach-san-pham-phu-trach */
    public function actionDanhSachSanPhamPhuTrach(){
        if($_POST['type'] == 'danh_sach_san_pham_phu_trach'){
            $sanPham = SanPham::findAll(['nhan_vien_phu_trach_id'=>$_POST['id'],'active'=>1,'trang_thai'=>SanPham::DANG_BAN]);
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'content' => $this->renderAjax('../san-pham/danh-sach-san-pham-phu-trach', [
                        'sanPham' => $sanPham,
                        ])
            ];
        } else {
            throw new HttpException(500, 'Không xác thực được dữ liệu');
        }
    }

    /**update-nhu-cau-khach-hang */
    public function actionUpdateNhuCauKhachHang(){
        if (isset($_POST['san_pham'])){
            $arr_quan = [];
            $arr_huong = [];

            $arr_dien_tich = [];
            $arr_gia_tu = [];
            $arr_gia_den = [];

            $arr_id_sp = [];
            $arr_ngay_xem = [];

            $list_san_pham_da_chon= SanPham::find()->andFilterWhere(['in', 'id', $_POST['san_pham']])->all();

            /** @var SanPham $san_pham */
            foreach ($list_san_pham_da_chon as $san_pham){
                if ($san_pham->quan_id != ''){
                    $arr_quan[] = $san_pham->quan_id;
                }
                if (($san_pham->huong) != ''){
                    $arr_huong[] = $san_pham->huong;
                }
                $arr_dien_tich[] = $san_pham->dien_tich;
                $arr_gia_tu[] = $san_pham->gia_tu;
                $arr_gia_den[] = $san_pham->gia_den;
                $arr_id_sp[] = $san_pham->id;
                $arr_ngay_xem[] = $_POST['ngay_xem'][$san_pham->id];
            }
//            $ngay_xem = implode(', ',array_filter($arr_ngay_xem));
            $ngay_xem = implode(', ',$arr_ngay_xem);
            $id_san_pham = implode(', ',$arr_id_sp);
            $string_huong = implode(', ',$arr_huong);

            $huong = array_values(array_unique(explode(', ',$string_huong)));
//            VarDumper::dump($huong,10,true); exit();
            $dien_tich_tu = count($arr_dien_tich) > 0 ? min($arr_dien_tich) : 0;
            $dien_tich_den = count($arr_dien_tich) > 0 ? max($arr_dien_tich) : 0;
            $gia_tu = count($arr_gia_tu) > 0 ? min($arr_gia_tu) : 0;
            $gia_den = count($arr_gia_den) > 0 ? max($arr_gia_den) : 0;

            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'huong'=> $huong,
                'dien_tich_tu' => $dien_tich_tu,
                'dien_tich_den' => $dien_tich_den,
                'gia_tu' => $gia_tu,
                'gia_den' => $gia_den,
                'quan' => array_unique($arr_quan),
                'id_san_pham' => $id_san_pham,
                'ngay_xem' => $ngay_xem
            ];

        }else{
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'huong'=> 'false',
            ];
        }
    }

    /** danh-sach-san-pham-trong-kho */
    public function actionDanhSachSanPhamTrongKho(){
        if (isset($_POST['chu_nha'])){
            $chu_nha = $_POST['chu_nha'];
            $dia_chi = $_POST['dia_chi'];
            $data = SanPham::find()
                ->andWhere("active = 1 and
                     (chu_nha like '%{$chu_nha}%' and dia_chi like '%{$dia_chi}%')")->all();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'content' => $this->renderAjax('../san-pham/danh-sach-san-pham-trong-kho', [
                    'data' => $data,
                ])
            ];
        } else {
            throw new HttpException(500, 'Không xác thực được dữ liệu');
        }
    }

    /** luu-thong-tin-ban */
    public function actionLuuThongTinBan(){
        if (isset($_POST['LichSuDoanhThu'])){
            $lich_su_doanh_thu = LichSuDoanhThu::findOne(['san_pham_id' => $_POST['LichSuDoanhThu']['san_pham_id'], 'active' => LichSuDoanhThu::ACTIVE]);
            if(is_null($lich_su_doanh_thu)){
                $lich_su_doanh_thu = new LichSuDoanhThu();
            }

            $doanh_thu_hoa_hong = ($_POST['LichSuDoanhThu']['doanh_thu_hoa_hong'] == null ? 0 : $_POST['LichSuDoanhThu']['doanh_thu_hoa_hong']);
            $doanh_thu_hoa_hong = str_replace('.', '', $doanh_thu_hoa_hong);
            $doanh_thu_hoa_hong = floatval($doanh_thu_hoa_hong);

            $doanh_thu_da_nhan = $_POST['LichSuDoanhThu']['doanh_thu_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['doanh_thu_da_nhan'];
            $doanh_thu_da_nhan = str_replace('.', '', $doanh_thu_da_nhan);
            $doanh_thu_da_nhan = floatval($doanh_thu_da_nhan);

            $hoa_hong_ban = ($_POST['LichSuDoanhThu']['hoa_hong_ban'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_ban']);
            $hoa_hong_ban = str_replace('.', '', $hoa_hong_ban);
            $hoa_hong_ban = floatval($hoa_hong_ban);

            $hoa_hong_ban_da_nhan = $_POST['LichSuDoanhThu']['hoa_hong_ban_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_ban_da_nhan'];
            $hoa_hong_ban_da_nhan = str_replace('.', '', $hoa_hong_ban_da_nhan);
            $hoa_hong_ban_da_nhan = floatval($hoa_hong_ban_da_nhan);

            $hoa_hong_nhap = ($_POST['LichSuDoanhThu']['hoa_hong_nhap'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_nhap']);
            $hoa_hong_nhap = str_replace('.', '', $hoa_hong_nhap);
            $hoa_hong_nhap = floatval($hoa_hong_nhap);

            $hoa_hong_nhap_da_nhan = $_POST['LichSuDoanhThu']['hoa_hong_nhap_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_nhap_da_nhan'];
            $hoa_hong_nhap_da_nhan = str_replace('.', '', $hoa_hong_nhap_da_nhan);
            $hoa_hong_nhap_da_nhan = floatval($hoa_hong_nhap_da_nhan);

            $hoa_hong_chuyen_khach = ($_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach']);
            $hoa_hong_chuyen_khach = str_replace('.', '', $hoa_hong_chuyen_khach);
            $hoa_hong_chuyen_khach = floatval($hoa_hong_chuyen_khach);

            $hoa_hong_chuyen_khach_da_nhan = $_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach_da_nhan'];
            $hoa_hong_chuyen_khach_da_nhan = str_replace('.', '', $hoa_hong_chuyen_khach_da_nhan);
            $hoa_hong_chuyen_khach_da_nhan = floatval($hoa_hong_chuyen_khach_da_nhan);

            $ngay_thanh_toan = myAPI::convertDMY2YMD($_POST['LichSuDoanhThu']['ngay_thanh_toan']);

            $nguoi_thanh_toan = $_POST['LichSuDoanhThu']['nguoi_thanh_toan'];

            $lich_su_doanh_thu->load(Yii::$app->request->post());
            $lich_su_doanh_thu->doanh_thu_hoa_hong = $doanh_thu_hoa_hong;
            $lich_su_doanh_thu->doanh_thu_da_nhan = $doanh_thu_da_nhan;
            $lich_su_doanh_thu->hoa_hong_ban = $hoa_hong_ban;
            $lich_su_doanh_thu->hoa_hong_ban_da_nhan = $hoa_hong_ban_da_nhan;
            $lich_su_doanh_thu->hoa_hong_nhap = $hoa_hong_nhap;
            $lich_su_doanh_thu->hoa_hong_nhap_da_nhan = $hoa_hong_nhap_da_nhan;
            $lich_su_doanh_thu->hoa_hong_chuyen_khach = $hoa_hong_chuyen_khach;
            $lich_su_doanh_thu->hoa_hong_chuyen_khach_da_nhan = $hoa_hong_chuyen_khach_da_nhan;
            $lich_su_doanh_thu->ngay_thanh_toan = $ngay_thanh_toan;
            $lich_su_doanh_thu->ngay_ban = $lich_su_doanh_thu->sanPham->ngay_ban;
            $thong_tin = ThongTinBanHang::find()->andFilterWhere(['san_pham_id' => $lich_su_doanh_thu->sanPham->id,
                'active' => LichSuDoanhThu::ACTIVE])
                ->andWhere('nguoi_ban_id is not null')->one();
            if(!is_null($thong_tin)){
                $lich_su_doanh_thu->nguoi_ban_id = $thong_tin->nguoi_ban_id;
            }
            if(!$lich_su_doanh_thu->save()){
                throw new HttpException(500, Html::errorSummary($lich_su_doanh_thu));
            }

            $doanh_thu_da_lay = $doanh_thu_da_nhan == 0 ? $lich_su_doanh_thu->sanPham->doanh_thu_da_lay : $doanh_thu_da_nhan;

            // lưu lịch sử doanh thu
            $lich_su_doanh_thu->sanPham->updateAttributes([
                'doanh_thu_hoa_hong' => $doanh_thu_hoa_hong,
                'hoa_hong_nhan_vien' => $hoa_hong_ban,
                'hoa_hong_nhap_hang' => $hoa_hong_nhap,
                'loi_nhuan' => $doanh_thu_hoa_hong - $hoa_hong_ban - $hoa_hong_nhap - $hoa_hong_chuyen_khach,
                'ngay_thanh_toan' => $ngay_thanh_toan,
                'nguoi_thanh_toan' => $nguoi_thanh_toan,
                'doanh_thu_da_lay' => $doanh_thu_da_lay,
                'doanh_thu_con_lai' => $doanh_thu_hoa_hong - $doanh_thu_da_lay,
                'hoa_hong_chuyen_khach' => $hoa_hong_chuyen_khach,
                'chuyen_khach_id' => $_POST['LichSuDoanhThu']['nguoi_chuyen_id'],
                'da_thanh_toan_nhap_hang' => $hoa_hong_nhap_da_nhan,
                'da_lay_hoa_hong_chuyen_khach' => $hoa_hong_chuyen_khach_da_nhan,
                'da_thanh_toan_ban_hang' => $hoa_hong_ban_da_nhan,
            ]);

            $lich_su_doanh_thu->sanPham->updateAttributes([
                'con_lai_nhap_hang' => $lich_su_doanh_thu->sanPham->hoa_hong_nhap_hang - $hoa_hong_nhap_da_nhan,
                'con_lai_ban_hang' => $lich_su_doanh_thu->sanPham->hoa_hong_nhan_vien - $hoa_hong_ban_da_nhan,
                'con_lai_hoa_hong_chuyen_khach' => $lich_su_doanh_thu->sanPham->hoa_hong_chuyen_khach - $hoa_hong_chuyen_khach_da_nhan,
            ]);

            if ($hoa_hong_ban > 0){
                $thuong_ban_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                    'kieu' => ThuongPhat::BAN_HANG, 'lich_su_id' => $lich_su_doanh_thu->id, 'nhan_vien_id' => $lich_su_doanh_thu->sanPham->nhan_vien_ban_id])
                    ->one();
                if(!in_array($lich_su_doanh_thu->sanPham->trang_thai, [SanPham::DA_BAN, SanPham::DA_BAN_MOT_PHAN])){
                    $thong_tin_ban_hang = ThongTinBanHang::find()->andFilterWhere(['san_pham_id' => $lich_su_doanh_thu->sanPham->id])
                        ->andWhere('nguoi_ban_id is not null')->orderBy(['created' => SORT_DESC])->one();
                    if(!is_null($thong_tin_ban_hang)){
                        $lich_su_doanh_thu->sanPham->nhan_vien_ban_id = $thong_tin_ban_hang->nguoi_ban_id;
                    }
                }
                if ($lich_su_doanh_thu->sanPham->nhan_vien_ban_id != null){
                    if(is_null($thuong_ban_hang)) {
                        $thuong_ban_hang = new ThuongPhat();
                        $thuong_ban_hang->san_pham_id = $lich_su_doanh_thu->san_pham_id;
                        $thuong_ban_hang->kieu = ThuongPhat::BAN_HANG;
                        $thuong_ban_hang->type = ThuongPhat::THUONG;
                        $thuong_ban_hang->ngay_ap_dung = $lich_su_doanh_thu->sanPham->ngay_ban;
                        $thuong_ban_hang->nhan_vien_id = $lich_su_doanh_thu->sanPham->nhan_vien_ban_id;
                        $thuong_ban_hang->ghi_chu = $lich_su_doanh_thu->ghi_chu;
                        $thuong_ban_hang->lich_su_id = $lich_su_doanh_thu->id;
                    }
                    $thuong_ban_hang->chi_phi = $hoa_hong_ban;
                    $thuong_ban_hang->thuong_ban_da_lay = $hoa_hong_ban_da_nhan;
                    $thuong_ban_hang->thuong_ban_con_lai = $lich_su_doanh_thu->sanPham->con_lai_ban_hang;
                    if (!$thuong_ban_hang->save()) {
                        throw new HttpException(500, Html::errorSummary($thuong_ban_hang));
                    }
                }
            }else{
                $thuong_ban_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                    'kieu' => ThuongPhat::BAN_HANG, 'lich_su_id' => $lich_su_doanh_thu->id, 'nhan_vien_id' => $lich_su_doanh_thu->sanPham->nhan_vien_ban_id])
                    ->one();
                if(!is_null($thuong_ban_hang)){
                    $thuong_ban_hang->delete();
                }
            }

            if ($hoa_hong_nhap > 0) {
                $thuong_nhap_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                    'kieu' => ThuongPhat::NHAP_HANG, 'lich_su_id' => $lich_su_doanh_thu->id,
                    'nhan_vien_id' => $lich_su_doanh_thu->sanPham->nguoi_tao_id])->one();
                if ($lich_su_doanh_thu->sanPham->nguoi_tao_id != null) {
                    if(is_null($thuong_nhap_hang)) {
                        $thuong_nhap_hang = new ThuongPhat();
                        $thuong_nhap_hang->san_pham_id = $lich_su_doanh_thu->san_pham_id;
                        $thuong_nhap_hang->kieu = ThuongPhat::NHAP_HANG;
                        $thuong_nhap_hang->type = ThuongPhat::THUONG;
                        $thuong_nhap_hang->ngay_ap_dung = $lich_su_doanh_thu->sanPham->ngay_ban;
                        $thuong_nhap_hang->nhan_vien_id = $lich_su_doanh_thu->sanPham->nguoi_tao_id;
                        $thuong_nhap_hang->lich_su_id = $lich_su_doanh_thu->id;
                    }
                    $thuong_nhap_hang->chi_phi = $hoa_hong_nhap;
                    $thuong_nhap_hang->thuong_nhap_da_lay = $hoa_hong_nhap_da_nhan;
                    $thuong_nhap_hang->thuong_nhap_con_lai = $lich_su_doanh_thu->sanPham->con_lai_nhap_hang;
                    if (!$thuong_nhap_hang->save()) {
                        throw new HttpException(500, Html::errorSummary($thuong_nhap_hang));
                    }
                }
            }else{
                $thuong_nhap_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                    'kieu' => ThuongPhat::NHAP_HANG, 'lich_su_id' => $lich_su_doanh_thu->id,
                    'nhan_vien_id' => $lich_su_doanh_thu->sanPham->nguoi_tao_id])->one();
                if(!is_null($thuong_nhap_hang)){
                    $thuong_nhap_hang->delete();
                }
            }

            if ($hoa_hong_chuyen_khach > 0) {
                if ($lich_su_doanh_thu->sanPham->chuyen_khach_id != null && $lich_su_doanh_thu->sanPham->chuyen_khach_id != '') {
                    $nhan_vien_chuyen = User::findOne($lich_su_doanh_thu->sanPham->chuyen_khach_id);
                    if (!is_null($nhan_vien_chuyen)) {
                        $thuong_chuyen_khach = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                            'kieu' => ThuongPhat::CHUYEN_KHACH, 'lich_su_id' => $lich_su_doanh_thu->id,
                            'nhan_vien_id' => $lich_su_doanh_thu->sanPham->chuyen_khach_id])->one();
                        if (is_null($thuong_chuyen_khach)) {
                            $thuong_chuyen_khach = new ThuongPhat();
                            $thuong_chuyen_khach->san_pham_id = $lich_su_doanh_thu->san_pham_id;
                            $thuong_chuyen_khach->kieu = ThuongPhat::CHUYEN_KHACH;
                            $thuong_chuyen_khach->type = ThuongPhat::THUONG;
                            $thuong_chuyen_khach->ngay_ap_dung = $lich_su_doanh_thu->sanPham->ngay_ban;
                            $thuong_chuyen_khach->nhan_vien_id = $lich_su_doanh_thu->sanPham->chuyen_khach_id;
                            $thuong_chuyen_khach->lich_su_id = $lich_su_doanh_thu->id;
                        }
                        $thuong_chuyen_khach->chi_phi = $hoa_hong_chuyen_khach;
                        $thuong_chuyen_khach->thuong_chuyen_khach_da_lay = $hoa_hong_chuyen_khach_da_nhan;
                        $thuong_chuyen_khach->thuong_chuyen_khach_con_lai = $lich_su_doanh_thu->sanPham->con_lai_hoa_hong_chuyen_khach;
                        if (!$thuong_chuyen_khach->save()) {
                            throw new HttpException(500, Html::errorSummary($thuong_chuyen_khach));
                        }
                    }
                }
            }else{
                $thuong_chuyen_khach = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                    'kieu' => ThuongPhat::CHUYEN_KHACH, 'lich_su_id' => $lich_su_doanh_thu->id,
                    'nhan_vien_id' => $lich_su_doanh_thu->sanPham->chuyen_khach_id])->one();
                if(!is_null($thuong_chuyen_khach)){
                    $thuong_chuyen_khach->delete();
                }
            }


            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'content' => 'Đã cập nhật doanh thu thành công.',
                'title' => 'Thông báo'
            ];
        }else
            throw new HttpException(500,'Không nhận được thông tin sản phẩm!');
    }

    /** xoa-anh */
    public function actionXoaAnh(){
        $anh_sp = AnhSanPham::findOne($_POST['idSanPham']);
        if ($anh_sp->delete())
            echo Json::encode([
                'content'=> 'Xoá thành công',
                'title' => 'Xoá ảnh sản phẩm'
            ]);
        else
            throw new HttpException(500,Html::errorSummary($anh_sp));
        exit;

    }

    /** xoa-thong-tin-ban */
    public function actionXoaThongTinBan(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['id'])){
                $model = ThongTinBanHang::findOne($_POST['id']);
                $model->updateAttributes(['active' => 0]);
                $chiTietBan = ThongTinBanHang::find()->andFilterWhere(['trang_thai'=>TrangThaiBanHang::DA_MUA,'active' => 1, 'san_pham_id' => $model->san_pham_id])
                    ->andWhere('khach_hang_id is null')->all();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => 'Đã xóa thông tin bán hàng thành công',
                    'data' => $this->renderAjax('view_xem_thong_tin_ban', [
                        'chiTietBan' => $chiTietBan
                    ]),
                    'title' => 'Thông báo'
                ];
            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên. Vui lòng kiểm tra lại.');
            }
        }else{
            throw new NotFoundHttpException(500, 'Đường dẫn sai cú pháp. Vui lòng kiểm tra lại.');
        }
    }

    /** luu-thong-tin-ban-view */
    public function actionLuuThongTinBanView(){
        if(Yii::$app->request->isAjax){
            if (!is_null($_POST['san_pham_id'])){
                $san_pham = SanPham::findOne($_POST['san_pham_id']);
                if(is_null($san_pham)){
                    throw new HttpException(500, 'Sản phẩm không tồn tại');
                }
                $thong_tin_ban_hang = ThongTinBanHang::findOne($_POST['ThongTinBanHang']['id']);
                $thong_tin_ban_hang->ngay_ban = $_POST['ThongTinBanHang']['ngay_ban'];
                $thong_tin_ban_hang->so_tien = $_POST['ThongTinBanHang']['so_tien'];
                $thong_tin_ban_hang->ghi_chu = $_POST['ThongTinBanHang']['ghi_chu'];

                if ($thong_tin_ban_hang->kieu_nguoi_ban == ThongTinBanHang::NHAN_VIEN){
                    $thong_tin_ban_hang->nguoi_ban_id = $_POST['ThongTinBanHang']['nguoi_ban_id'];
                    $thong_tin_ban_hang->nguoi_ban = $thong_tin_ban_hang->nguoiBan->hoten;
                    LichSuDoanhThu::updateAll(['nguoi_ban_id' => $thong_tin_ban_hang->nguoi_ban_id],
                        ['active' => LichSuDoanhThu::ACTIVE, 'san_pham_id' => $_POST['san_pham_id']]);
                }elseif ($thong_tin_ban_hang->kieu_nguoi_ban == ThongTinBanHang::CHU_NHA){
                    $thong_tin_ban_hang->nguoi_ban_id = '';
                    $thong_tin_ban_hang->nguoi_ban = SanPham::findOne([$_POST['san_pham_id']])->chu_nha;
                }elseif ($thong_tin_ban_hang->kieu_nguoi_ban == SanPham::DON_VI_KHAC_BAN){
                    $thong_tin_ban_hang->nguoi_ban_id = '';
                    $thong_tin_ban_hang->nguoi_ban = $_POST['ThongTinBanHang']['nguoi_ban'];
                }
                if ($thong_tin_ban_hang->save()){
                    $ngay_ban = myAPI::convertDMY2YMD($_POST['ThongTinBanHang']['ngay_ban']);
                    if ($thong_tin_ban_hang->nguoi_ban_id == ''){
                        $thong_tin_ban_hang->sanPham->updateAttributes(
                            [
                                'nguoi_ban'=>$thong_tin_ban_hang->nguoi_ban,
                                'nhan_vien_ban_id'=>$thong_tin_ban_hang->nguoi_ban_id,
                                'trang_thai'=>SanPham::DA_MAT,
                                'type_nguoi_ban' => $_POST['ThongTinBanHang']['kieu_nguoi_ban'],
                                'ghi_chu' => $thong_tin_ban_hang->sanPham->ghi_chu,
                                'ngay_ban' => $ngay_ban,
                            ]
                        );
                    }else{
                        $thong_tin_ban_hang->sanPham->updateAttributes(
                            [
                                'nguoi_ban'=>$thong_tin_ban_hang->nguoi_ban,
                                'nhan_vien_ban_id'=>$thong_tin_ban_hang->nguoi_ban_id,
                                'trang_thai'=>$_POST['ThongTinBanHang']['trang_thai'],
                                'type_nguoi_ban' => $_POST['ThongTinBanHang']['kieu_nguoi_ban'],
                                'ghi_chu' => $thong_tin_ban_hang->sanPham->ghi_chu,
                                'ngay_ban' => $ngay_ban,
                            ]
                        );
                    }

                    $chiTietBan = ThongTinBanHang::find()->andFilterWhere(['trang_thai'=>TrangThaiBanHang::DA_MUA,'active' => 1, 'san_pham_id' => $san_pham->id])
                        ->andWhere('khach_hang_id is null')->all();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => 'Đã xóa thông tin bán hàng thành công',
                        'data' => $this->renderAjax('view_xem_thong_tin_ban', [
                            'chiTietBan' => $chiTietBan
                        ]),
                        'title' => 'Thông báo'
                    ];
                }else
                    throw new HttpException(500, Html::errorSummary($thong_tin_ban_hang));
            }else{
                throw new HttpException(500, 'Không có thông tin sản phẩm');
            }
        }else{
            throw new NotFoundHttpException(500, 'Đường dẫn sai cú pháp.');
        }
    }

    /** cap-nhat-doanh-thu */
    public function actionCapNhatDoanhThu(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['LichSuDoanhThu'])){
                $model = LichSuDoanhThu::findOne($_POST['LichSuDoanhThu']['id']);
                if(is_null($model)){
                    throw new HttpException(500, 'Dữ liệu không tồn tại. Vui lòng kiểm tra lại.');
                }

                $doanh_thu_hoa_hong = ($_POST['LichSuDoanhThu']['doanh_thu_hoa_hong'] == null ? 0 : $_POST['LichSuDoanhThu']['doanh_thu_hoa_hong']);
                $doanh_thu_hoa_hong = str_replace('.', '', $doanh_thu_hoa_hong);
                $doanh_thu_hoa_hong = floatval($doanh_thu_hoa_hong);

                $doanh_thu_da_nhan = $_POST['LichSuDoanhThu']['doanh_thu_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['doanh_thu_da_nhan'];
                $doanh_thu_da_nhan = str_replace('.', '', $doanh_thu_da_nhan);
                $doanh_thu_da_nhan = floatval($doanh_thu_da_nhan);

                $hoa_hong_ban = ($_POST['LichSuDoanhThu']['hoa_hong_ban'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_ban']);
                $hoa_hong_ban = str_replace('.', '', $hoa_hong_ban);
                $hoa_hong_ban = floatval($hoa_hong_ban);

                $hoa_hong_ban_da_nhan = $_POST['LichSuDoanhThu']['hoa_hong_ban_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_ban_da_nhan'];
                $hoa_hong_ban_da_nhan = str_replace('.', '', $hoa_hong_ban_da_nhan);
                $hoa_hong_ban_da_nhan = floatval($hoa_hong_ban_da_nhan);

                $hoa_hong_nhap = ($_POST['LichSuDoanhThu']['hoa_hong_nhap'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_nhap']);
                $hoa_hong_nhap = str_replace('.', '', $hoa_hong_nhap);
                $hoa_hong_nhap = floatval($hoa_hong_nhap);

                $hoa_hong_nhap_da_nhan = $_POST['LichSuDoanhThu']['hoa_hong_nhap_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_nhap_da_nhan'];
                $hoa_hong_nhap_da_nhan = str_replace('.', '', $hoa_hong_nhap_da_nhan);
                $hoa_hong_nhap_da_nhan = floatval($hoa_hong_nhap_da_nhan);

                $hoa_hong_chuyen_khach = ($_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach']);
                $hoa_hong_chuyen_khach = str_replace('.', '', $hoa_hong_chuyen_khach);
                $hoa_hong_chuyen_khach = floatval($hoa_hong_chuyen_khach);

                $hoa_hong_chuyen_khach_da_nhan = $_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach_da_nhan'] == null ? 0 : $_POST['LichSuDoanhThu']['hoa_hong_chuyen_khach_da_nhan'];
                $hoa_hong_chuyen_khach_da_nhan = str_replace('.', '', $hoa_hong_chuyen_khach_da_nhan);
                $hoa_hong_chuyen_khach_da_nhan = floatval($hoa_hong_chuyen_khach_da_nhan);

                $ngay_thanh_toan = myAPI::convertDMY2YMD($_POST['LichSuDoanhThu']['ngay_thanh_toan']);

                $nguoi_thanh_toan = $_POST['LichSuDoanhThu']['nguoi_thanh_toan'];

                $model->load(Yii::$app->request->post());
                $model->doanh_thu_hoa_hong = $doanh_thu_hoa_hong;
                $model->doanh_thu_da_nhan = $doanh_thu_da_nhan;
                $model->hoa_hong_ban = $hoa_hong_ban;
                $model->hoa_hong_ban_da_nhan = $hoa_hong_ban_da_nhan;
                $model->hoa_hong_nhap = $hoa_hong_nhap;
                $model->hoa_hong_nhap_da_nhan = $hoa_hong_nhap_da_nhan;
                $model->hoa_hong_chuyen_khach = $hoa_hong_chuyen_khach;
                $model->hoa_hong_chuyen_khach_da_nhan = $hoa_hong_chuyen_khach_da_nhan;
                $model->ngay_thanh_toan = $ngay_thanh_toan;
                if(!$model->save()){
                    throw new HttpException(500, Html::errorSummary($model));
                }

                if($model->active == LichSuDoanhThu::ACTIVE) {
                    $doanh_thu_da_lay = $doanh_thu_da_nhan == 0 ? $model->sanPham->doanh_thu_da_lay : $doanh_thu_da_nhan;

                    // lưu lịch sử doanh thu
                    $model->sanPham->updateAttributes([
                        'doanh_thu_hoa_hong' => $doanh_thu_hoa_hong,
                        'hoa_hong_nhan_vien' => $hoa_hong_ban,
                        'hoa_hong_nhap_hang' => $hoa_hong_nhap,
                        'loi_nhuan' => $doanh_thu_hoa_hong - $hoa_hong_ban - $hoa_hong_nhap - $hoa_hong_chuyen_khach,
                        'ngay_thanh_toan' => $ngay_thanh_toan,
                        'nguoi_thanh_toan' => $nguoi_thanh_toan,
                        'doanh_thu_da_lay' => $doanh_thu_da_lay,
                        'doanh_thu_con_lai' => $doanh_thu_hoa_hong - $doanh_thu_da_lay,
                        'hoa_hong_chuyen_khach' => $hoa_hong_chuyen_khach,
                        'chuyen_khach_id' => $_POST['LichSuDoanhThu']['nguoi_chuyen_id'],
                        'da_thanh_toan_nhap_hang' => $hoa_hong_nhap_da_nhan,
                        'da_lay_hoa_hong_chuyen_khach' => $hoa_hong_chuyen_khach_da_nhan,
                        'da_thanh_toan_ban_hang' => $hoa_hong_ban_da_nhan,
                    ]);

                    $model->sanPham->updateAttributes([
                        'con_lai_nhap_hang' => $model->sanPham->hoa_hong_nhap_hang - $hoa_hong_nhap_da_nhan,
                        'con_lai_ban_hang' => $model->sanPham->hoa_hong_nhan_vien - $hoa_hong_ban_da_nhan,
                        'con_lai_hoa_hong_chuyen_khach' => $model->sanPham->hoa_hong_chuyen_khach - $hoa_hong_chuyen_khach_da_nhan,
                    ]);
                }

                if(!in_array($model->sanPham->trang_thai, [SanPham::DA_BAN, SanPham::DA_BAN_MOT_PHAN])){
                    $thong_tin_ban_hang = ThongTinBanHang::find()->andFilterWhere(['san_pham_id' => $model->sanPham->id])
                        ->andWhere('nguoi_ban_id is not null')->orderBy(['created' => SORT_DESC])->one();
                    if(!is_null($thong_tin_ban_hang)){
                        $model->sanPham->nhan_vien_ban_id = $thong_tin_ban_hang->nguoi_ban_id;
                        $model->sanPham->ngay_ban = $thong_tin_ban_hang->ngay_ban;
                    }
                }

                if ($hoa_hong_ban > 0){
                    $thuong_ban_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                        'kieu' => ThuongPhat::BAN_HANG, 'lich_su_id' => $model->id])
                        ->andWhere('nhan_vien_id is not null')
                        ->one();
                    if ($model->sanPham->nhan_vien_ban_id != null){
                        if(is_null($thuong_ban_hang)) {
                            $thuong_ban_hang = new ThuongPhat();
                            $thuong_ban_hang->san_pham_id = $model->san_pham_id;
                            $thuong_ban_hang->kieu = ThuongPhat::BAN_HANG;
                            $thuong_ban_hang->type = ThuongPhat::THUONG;
                            $thuong_ban_hang->ngay_ap_dung = $model->sanPham->ngay_ban;
                            $thuong_ban_hang->nhan_vien_id = $model->sanPham->nhan_vien_ban_id;
                            $thuong_ban_hang->ghi_chu = $model->sanPham->ghi_chu;
                            $thuong_ban_hang->lich_su_id = $model->id;
                        }
                        $thuong_ban_hang->chi_phi = $hoa_hong_ban;
                        $thuong_ban_hang->thuong_ban_da_lay = $hoa_hong_ban_da_nhan;
                        $thuong_ban_hang->thuong_ban_con_lai = $model->sanPham->con_lai_ban_hang;
                        if (!$thuong_ban_hang->save()) {
                            throw new HttpException(500, Html::errorSummary($thuong_ban_hang));
                        }
                    }
                }else{
                    $thuong_ban_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                        'kieu' => ThuongPhat::BAN_HANG, 'lich_su_id' => $model->id])
                        ->andWhere('nhan_vien_id is not null')
                        ->one();
                    if(!is_null($thuong_ban_hang)){
                        $thuong_ban_hang->delete();
                    }
                }

                if ($hoa_hong_nhap > 0) {
                    $thuong_nhap_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                        'kieu' => ThuongPhat::NHAP_HANG, 'lich_su_id' => $model->id])->one();
                    if ($model->sanPham->nguoi_tao_id != null) {
                        if(is_null($thuong_nhap_hang)) {
                            $thuong_nhap_hang = new ThuongPhat();
                            $thuong_nhap_hang->san_pham_id = $model->san_pham_id;
                            $thuong_nhap_hang->kieu = ThuongPhat::NHAP_HANG;
                            $thuong_nhap_hang->type = ThuongPhat::THUONG;
                            $thuong_nhap_hang->ngay_ap_dung = $model->sanPham->ngay_ban;
                            $thuong_nhap_hang->nhan_vien_id = $model->sanPham->nguoi_tao_id;
                            $thuong_nhap_hang->lich_su_id = $model->id;
                        }
                        $thuong_nhap_hang->chi_phi = $hoa_hong_nhap;
                        $thuong_nhap_hang->thuong_nhap_da_lay = $hoa_hong_nhap_da_nhan;
                        $thuong_nhap_hang->thuong_nhap_con_lai = $model->sanPham->con_lai_nhap_hang;
                        if (!$thuong_nhap_hang->save()) {
                            throw new HttpException(500, Html::errorSummary($thuong_nhap_hang));
                        }
                    }
                }else{
                    $thuong_nhap_hang = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                        'kieu' => ThuongPhat::NHAP_HANG, 'lich_su_id' => $model->id])->one();
                    if(!is_null($thuong_nhap_hang)){
                        $thuong_nhap_hang->delete();
                    }
                }

                if ($hoa_hong_chuyen_khach > 0) {
                    if ($model->nguoi_chuyen_id != null && $model->nguoi_chuyen_id != '') {
                        $nhan_vien_chuyen = User::findOne($model->nguoi_chuyen_id);
                        if (!is_null($nhan_vien_chuyen)) {
                            $thuong_chuyen_khach = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                                'kieu' => ThuongPhat::CHUYEN_KHACH, 'lich_su_id' => $model->id])->one();
                            if (is_null($thuong_chuyen_khach)) {
                                $thuong_chuyen_khach = new ThuongPhat();
                                $thuong_chuyen_khach->san_pham_id = $model->san_pham_id;
                                $thuong_chuyen_khach->kieu = ThuongPhat::CHUYEN_KHACH;
                                $thuong_chuyen_khach->type = ThuongPhat::THUONG;
                                $thuong_chuyen_khach->ngay_ap_dung = $model->sanPham->ngay_ban;
                                $thuong_chuyen_khach->nhan_vien_id = $model->sanPham->chuyen_khach_id;
                                $thuong_chuyen_khach->lich_su_id = $model->id;
                            }
                            $thuong_chuyen_khach->chi_phi = $hoa_hong_chuyen_khach;
                            $thuong_chuyen_khach->thuong_chuyen_khach_da_lay = $hoa_hong_chuyen_khach_da_nhan;
                            $thuong_chuyen_khach->thuong_chuyen_khach_con_lai = $model->sanPham->con_lai_hoa_hong_chuyen_khach;
                            if (!$thuong_chuyen_khach->save()) {
                                throw new HttpException(500, Html::errorSummary($thuong_chuyen_khach));
                            }
                        }
                    }
                }else{
                    $thuong_chuyen_khach = ThuongPhat::find()->andFilterWhere(['type' => ThuongPhat::THUONG,
                        'kieu' => ThuongPhat::CHUYEN_KHACH, 'lich_su_id' => $model->id])->one();
                    if(!is_null($thuong_chuyen_khach)){
                        $thuong_chuyen_khach->delete();
                    }
                }

                $lsDoanhThu = LichSuDoanhThu::find()->andFilterWhere(['san_pham_id' => $model->san_pham_id])->all();
                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => 'Đã cập nhật doanh thu thành công.',
                    'title' => 'Thông báo',
                    'data' => $this->renderAjax('view_xem_lich_su_doanh_thu.php', [
                        'lsDoanhThu' => $lsDoanhThu
                    ]),
                ];
            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên.');
            }
        }else{
            throw new NotFoundHttpException(500, 'Đường dẫn không chính xác. Vui lòng thử lại.');
        }
    }

    /** xoa-lich-su-doanh-thu */
    public function actionXoaLichSuDoanhThu(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['id'])){
                ThuongPhat::deleteAll(['lich_su_id' => $_POST['id']]);
                $ls = LichSuDoanhThu::findOne($_POST['id']);
                if($ls->delete()){
                    $lsDoanhThu = LichSuDoanhThu::find()->andFilterWhere(['san_pham_id' => $ls->san_pham_id])->all();
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => 'Đã xóa thông tin bán hàng thành công',
                        'data' => $this->renderAjax('view_xem_lich_su_doanh_thu.php', [
                            'lsDoanhThu' => $lsDoanhThu
                        ]),
                        'title' => 'Thông báo'
                    ];
                }else{
                    throw new HttpException(500, Html::errorSummary($ls));
                }
            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu gửi lên. Vui lòng kiểm tra lại.');
            }
        }else{
            throw new NotFoundHttpException(500, 'Đường dẫ sai cú pháp. Vui lòng kiểm tra lại.');
        }
    }

    /** search */
    public function actionSearch(){
        $request = Yii::$app->request;
        $model = new QuanLySanPhamSearch();

        $cau_hinh = CauHinh::findOne(['ghi_chu'=>'khoang_gia']);
        $arrCauHinh  = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        $khoang_gias = [];

        foreach ($arrCauHinh as $item){
            $khoang_gias[trim($item)] = trim($item) ;
        }

        $nhan_viens =  ArrayHelper::map(\common\models\User::find()
            ->andFilterWhere(['status'=>10,'active'=>1,'nhom'=>\common\models\User::THANH_VIEN])->all(), 'id', 'hoten');
        $quan_huyens = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN])->all(), 'name', 'name');

        $view = '_search';
        if(!User::isUpdateAll())
            $view = '_search_nhan_vien';
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post())) {
                $search['QuanLySanPhamSearch'] = $request->post()['QuanLySanPhamSearch'];
                $search['r'] = 'category/quan-ly-san-pham';
                $search['_pjax'] = '#crud-datatable-pjax';
                Yii::$app->session->set('searchQuanLySanPham', $search);
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax($view, [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                        'nhan_viens' => $nhan_viens,
                        'quan_huyens' => $quan_huyens,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]).
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                return [
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax($view, [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                        'nhan_viens' => $nhan_viens,
                        'quan_huyens' => $quan_huyens
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** search-da-ban */
    public function actionSearchDaBan(){
        $request = Yii::$app->request;
        $model = new QuanLySanPhamSearch();

        $cau_hinh = CauHinh::findOne(['ghi_chu'=>'khoang_gia']);
        $arrCauHinh  = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        $khoang_gias = [];

        foreach ($arrCauHinh as $item){
            $khoang_gias[trim($item)] = trim($item) ;
        }
        $quan_huyens = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN])->all(), 'name', 'name');

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post())) {
                $search['QuanLySanPhamSearch'] = $request->post()['QuanLySanPhamSearch'];
                $search['r'] = 'category/quan-ly-san-pham';
                $search['_pjax'] = '#crud-datatable-pjax';
                Yii::$app->session->set('searchQuanLySanPhamDaBan', $search);
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax('san-pham-da-ban/_search', [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                        'quan_huyens' => $quan_huyens,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]).
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                return [
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax('san-pham-da-ban/_search', [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                        'quan_huyens' => $quan_huyens,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** search-cho-duyet */
    public function actionSearchChoDuyet(){
        $request = Yii::$app->request;
        $model = new QuanLySanPhamSearch();

        $cau_hinh = CauHinh::findOne(['ghi_chu'=>'khoang_gia']);
        $arrCauHinh  = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        $khoang_gias = [];
        foreach ($arrCauHinh as $item){
            $khoang_gias[trim($item)] = trim($item) ;
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post())) {
                $search['QuanLySanPhamSearch'] = $request->post()['QuanLySanPhamSearch'];
                $search['r'] = 'category/quan-ly-san-pham';
                $search['_pjax'] = '#crud-datatable-pjax';
                Yii::$app->session->set('searchQuanLySanPhamChoDuyet', $search);
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax('san-pham-cho-duyet/_search', [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]).
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                return [
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax('san-pham-cho-duyet/_search', [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** search-phu-trach */
    public function actionSearchPhuTrach(){
        $request = Yii::$app->request;
        $model = new QuanLySanPhamSearch();

        $cau_hinh = CauHinh::findOne(['ghi_chu'=>'khoang_gia']);
        $arrCauHinh  = array_filter(explode('<br />', nl2br($cau_hinh->content)));
        $khoang_gias = [];
        foreach ($arrCauHinh as $item){
            $khoang_gias[trim($item)] = trim($item) ;
        }
        $nhan_viens =  ArrayHelper::map(\common\models\User::find()
            ->andFilterWhere(['status'=>10,'active'=>1,'nhom'=>\common\models\User::THANH_VIEN])->all(), 'id', 'hoten');
        $quan_huyens = ArrayHelper::map(DanhMuc::find()->andFilterWhere(['type' => DanhMuc::QUAN_HUYEN])->all(), 'name', 'name');

        $view = 'san-pham-phu-trach/_search';
        if(!User::isUpdateAll())
            $view = 'san-pham-phu-trach/_search_nhan_vien';

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post())) {
                $search['QuanLySanPhamSearch'] = $request->post()['QuanLySanPhamSearch'];
                $search['r'] = 'category/quan-ly-san-pham';
                $search['_pjax'] = '#crud-datatable-pjax';
                Yii::$app->session->set('searchQuanLySanPhamPhuTrach', $search);
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax($view, [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                        'nhan_viens' => $nhan_viens,
                        'quan_huyens' => $quan_huyens,
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]).
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                return [
                    'title' => "Tìm kiếm ",
                    'content' => $this->renderAjax($view, [
                        'model' => $model,
                        'khoang_gias' => $khoang_gias,
                        'nhan_viens' => $nhan_viens,
                        'quan_huyens' => $quan_huyens
                    ]),
                    'footer' => Html::button('<i class="fa fa-close"></i> Đóng lại', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('<i class="fa fa-search"></i> Tìm kiếm', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** xoa-nhieu-san-pham */
    public function actionXoaNhieuSanPham(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['NhanVien'])){
                $nid = [];
                $sanPhams = SanPham::find()->andFilterWhere(['in', 'id', $_POST['NhanVien']])->all();
                foreach ($sanPhams as $sanPham){
                    /** @var $sanPham SanPham */
                    $nid[] = $sanPham->nid;
                    $user = QuanLyUser::find()->andFilterWhere(['id'=>Yii::$app->user->id])->andWhere("vai_tro like '%Trưởng phòng%'")->one();
                    if($sanPham->trang_thai === SanPham::CHO_DUYET || User::isViewAll() == true || $user != null) {
                        $sanPham->updateAttributes(['active' => 0]);
                    }else{
                        throw new HttpException(500,'Sản phẩm đang trong trạng thái không thể xoá');
                    }
                }

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => 'Đã xóa sản phẩm thành công',
                    'title' => 'Thông báo',
                    'nid' => $nid
                ];
            }else{
                throw new HttpException(500, 'Không xác thực dữ liệu');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp');
        }
    }

    /** san-pham-het-han */
    public function actionSanPhamHetHan(){
        $searchModel = new QuanLySanPhamSearch();
        $dataProvider = $searchModel->searchHetHan(Yii::$app->request->queryParams);

        if(isset($_GET['QuanLySanPhamSearch']))
            Yii::$app->session->set('params_SanPham', $_GET['QuanLySanPhamSearch']);
        else{
            if(Yii::$app->session->get('params_SanPham'))
                $searchModel->attributes = Yii::$app->session->get('params_SanPham');
        }

        return $this->render('san-pham-het-han/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /** luu-gia-han */
    public function actionLuuGiaHan(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['GiaoDich']) && isset($_POST['ngay_gia_han'])){
                $sanPham = $this->findModel($_POST['GiaoDich']['san_pham_id']);

                $giaoDich = new GiaoDich();
                $giaoDich->load(Yii::$app->request->post());
                $giaoDich->type = GiaoDich::GIA_HAN;
                $giaoDich->nguoi_thuc_hien_giao_dich_id = Yii::$app->user->id;
                $giaoDich->nguoi_nhan_id = $sanPham->nguoi_tao_id;
                $giaoDich->noi_dung = 'Gia hạn sản phẩm #'.$sanPham->id;
                if($giaoDich->save()){
                    $giaoDich->sanPham->updateAttributes([
                        'active' => 1,
                        'het_han' => 0,
                        'han_dang' => myAPI::convertDateSaveIntoDb($_POST['ngay_gia_han']),
                    ]);

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'title' => 'Gia hạn sản phẩm',
                        'content' => 'Đã gia hạn sản phẩm đến '.$_POST['ngay_gia_han']
                    ];
                }else{
                    throw new HttpException(500 , Html::errorSummary($giaoDich));
                }
            }
        }else{
            throw new HttpException(500, 'Đường dẫn sai cú pháp');
        }
    }

    protected function findModel($id)
    {
        if (($model = SanPham::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
