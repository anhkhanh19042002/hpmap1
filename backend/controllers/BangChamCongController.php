<?php

namespace backend\controllers;

use backend\models\TrangThaiNghi;
use backend\models\UserVaiTro;
use backend\models\Vaitrouser;
use common\models\myAPI;
use common\models\User;
use Yii;
use backend\models\BangChamCong;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

class BangChamCongController extends CoreController
{
    public function behaviors()
    {
        $arr_action = ['index','load','update-heso', 'ly-do-nghi'];
        $rules = [];
        foreach ($arr_action as $item) {
            $rules[] = [
                'actions' => [$item],
                'allow' => true,
                'matchCallback' => function ($rule, $action) {
                    $action_name =  strtolower(str_replace('action', '', $action->id));
                    return myAPI::isAccess2('TaiChinh', $action_name);
                }
            ];
        }

        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' =>$rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /** index */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /** view */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /** created */
    public function actionCreate()
    {
        $model = new BangChamCong();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /** update */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /** delete */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /** load */
    public function actionLoad()
    {
        if(Yii::$app->request->isAjax) {
            if (isset($_POST['year']) && isset($_POST['month'])) {
                $year = $_POST['year'];
                $month = $_POST['month'] + 1;
                $content = $this->bangChamCong($month, $year);

                Yii::$app->response->format = Response::FORMAT_JSON;
                return [
                    'content' => $content,
                ];
            } else {
                throw new HttpException(500, 'Không xác thực được dữ liệu');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp. Vui lòng kiểm tra.');
        }
    }

    /** update-heso */
    public function actionUpdateHeso(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['id']) && isset($_POST['heso']) && isset($_POST['check'])){
                $data = explode('/', $_POST['id']);
                $bang_cham_cong = BangChamCong::findOne(['ngay' => $data[0], 'nhan_vien_id' => $data[1]]);
                if($_POST['check'] == 'true') {
                    if (is_null($bang_cham_cong)) {
                        $model = new BangChamCong();
                        $model->nhan_vien_id = $data[1];
                        $model->ngay = $data[0];
                        if($_POST['heso']==0){
                            $model->sang=date("Y-m-d H:i:s");
                        }
                        else
                        if($_POST['heso']==1){
                            $model->chieu=date("Y-m-d H:i:s");
                        }
                        if (!$model->save()) {
                            throw new HttpException(500, Html::errorSummary($model));
                        } else {
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'content' => 'Đã cập nhật hệ số thành công 1',
                            ];
                        }
                    } else {
                        if($_POST['heso']==0){
                            $bang_cham_cong->updateAttributes(['sang' => date("Y-m-d H:i:s"), 'updated' => date("Y-m-d H:i:s")]);
                            $bang_cham_cong->updateAttributes(['ly_do_nghi' => null, 'updated' => date("Y-m-d H:i:s")]);

                        }
                        else if($_POST['heso']==1){
                            $bang_cham_cong->updateAttributes(['chieu' => date("Y-m-d H:i:s"), 'updated' => date("Y-m-d H:i:s")]);
                            $bang_cham_cong->updateAttributes(['ly_do_nghi' => null, 'updated' => date("Y-m-d H:i:s")]);
                            }
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'content' => 'Đã cập nhật hệ số thành công',
                        ];
                    }
                }else{
                    if($_POST['heso']==0){
                        $bang_cham_cong->updateAttributes(['sang' => null, 'updated' => date("Y-m-d H:i:s")]);
                    }
                    else if($_POST['heso']==1){
                        $bang_cham_cong->updateAttributes(['chieu' => null, 'updated' => date("Y-m-d H:i:s")]);
                    }
                    if(!isset($bang_cham_cong->sang)&&!isset($bang_cham_cong->chieu)&&!isset($bang_cham_cong->ly_do_nghi)) {
                            $bang_cham_cong->delete();
                   }
                }
            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp. Vui lòng kiểm tra.');
        }
    }

    /** ly-do-nghi */
    public function actionLyDoNghi(){
        if(Yii::$app->request->isAjax){
            if(isset($_POST['data']) && isset($_POST['BangChamCong']) && isset($_POST['heso'])){
                $data = explode('/', $_POST['data']);
                $model = new BangChamCong();
                $model->nhan_vien_id = $data[1];
                $model->ngay = $data[0];
                if($_POST['BangChamCong']['trang_thai_nghi']==BangChamCong::NGHI_CO_LY_DO)
                {
                    if(User::getSoNgayNghi(date('m'),$data[1])>0)
                    {
                        $model->sang=date("Y-m-d H:i:s");
                        $model->chieu=date("Y-m-d H:i:s");
                    }
                    $model->ly_do_nghi = $_POST['BangChamCong']['ly_do_nghi'];
                }
                if(!$model->save()){
                    throw new HttpException(500, Html::errorSummary($model));
                }else{
                    if($_POST['BangChamCong']['trang_thai_nghi']==BangChamCong::NGHI_CO_LY_DO)
                    {
                        $so_lan_nghi= TrangThaiNghi::findOne(['user_id'=>$data[1],'thang'=>date('m')]);
                        if(User::getSoNgayNghi(date('m'),$data[1])>0)
                        {
                            $so_lan_nghi->updateAttributes(['so_lan_nghi_con_lai' => $so_lan_nghi->so_lan_nghi_con_lai-1, 'update_ngay_nghi' => date("Y-m-d H:i:s")]);
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'content'=>'Thêm lí do thành công',
                                'ly_do'=>$_POST['BangChamCong']['ly_do_nghi'],
                                'title'=>'Thông báo',
                                'status'=>1,
                            ];
                        }
                        else
                        {
                            Yii::$app->response->format = Response::FORMAT_JSON;
                            return [
                                'content'=>'Thêm lí do thành công',
                                'ly_do'=>$_POST['BangChamCong']['ly_do_nghi'],
                                'title'=>'Thông báo',
                                'status'=>2,
                            ];
                        }

                    }
                    else
                    {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return [
                            'status'=>0,
                        ];
                    }

                    Yii::$app->response->format = Response::FORMAT_JSON;
                    return [
                        'content' => 'Đã thêm lý do nghỉ thành công',
                    ];
                }
            }else{
                throw new HttpException(500, 'Không xác thực được dữ liệu');
            }
        }else{
            throw new NotFoundHttpException('Đường dẫn sai cú pháp. Vui lòng kiểm tra.');
        }
    }

    protected function findModel($id)
    {
        if (($model = BangChamCong::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Thông tin tìm kiếm không tồn tại. Vui lòng kiểm tra.');
    }

    public function bangChamCong($month, $year){
        $data_cham_congs = [];
        $thoi_gians = [];
        $days = [];
        $thoi_gian_tu = date('Y-m-d', strtotime($year.'-'.$month.'-1'));
        $thoi_gian_den = date('Y-m-t', strtotime($year.'-'.$month.'-1'));
        $users = User::find()
            ->andFilterWhere(['<>','id',1])
            ->andFilterWhere(['and', ['nhom' => User::THANH_VIEN, 'active' => 1], ['<=', 'date(created_at)', $thoi_gian_den]])
            ->orFilterWhere(['and', ['nhom' => User::THANH_VIEN, 'active' => 0],
                ['<=', 'date(created_at)', $thoi_gian_den], ['>=', 'date(ngay_nghi)', $thoi_gian_tu]])
            ->all();
        while ($thoi_gian_tu <= $thoi_gian_den){
            $days[] = date("d", strtotime($thoi_gian_tu));
            $thoi_gians[] = $thoi_gian_tu;
            $thoi_gian_tu = date("Y-m-d", strtotime("+1 day", strtotime($thoi_gian_tu)));
        }

        foreach ($users as $user){
            /** @var $user User */
            /** @var $user_vai_tro UserVaiTro */
            $cham_cong = [];
            foreach ($thoi_gians as $thoi_gian){
                $tempt = [];
                $tempt['he_so'] = $user->getHeSoLuongTheoNgay($thoi_gian);
                $tempt['thoi_gian'] = $thoi_gian;
                $tempt['id_nhan_vien'] = $user->id;
                $ly_do_nghi=$user->LiDoNGhi($thoi_gian);
                $tempt['ly_do_nghi'] = $ly_do_nghi;
                $cham_cong[] = $tempt;
                $thanhvien=$user->hoten;


            }
            $data_cham_congs[$thanhvien] = $cham_cong;
        }

        return $this->renderAjax('_bang_cham_cong', [
            'data_cham_congs' => $data_cham_congs,
            'days' => $days
        ]);
    }
}
