<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\BangChamCong;
use common\models\User;
/* @var $this yii\web\View */
/* @var $model backend\models\BangChamCong */
/* @var $form yii\widgets\ActiveForm */
/* @var $data string */
/* @var $heso string */
?>

<div class="ly-do-nghi-form">

    <?php $form = ActiveForm::begin(['options' => ['id' => 'form-ly-do-nghi']]); ?>

    <?= Html::hiddenInput('data', $data) ?>
    <?= Html::hiddenInput('heso', $heso) ?>

        <div>
            <label>Số lần nghỉ còn lại: <span style="color: red"><?=User::getSoNgayNghi(date('m'),explode('/',$_POST['id'])[1])?></span></label>
        </div>
        <div>
            <?=$form->field($model,'trang_thai_nghi')->dropDownList([
                BangChamCong::NGHI_CO_LY_DO => BangChamCong::NGHI_CO_LY_DO,
                BangChamCong::NGHI_KHONG_LY_DO => BangChamCong::NGHI_KHONG_LY_DO,
            ])?>
        </div>



    <?= $form->field($model, 'ly_do_nghi')->textarea(['rows' => 3]) ?>

    <?php ActiveForm::end(); ?>

</div>
