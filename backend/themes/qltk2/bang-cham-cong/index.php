<?php
$this->title = 'Bảng chấm công';

use yii\helpers\Html; ?>
<?=\yii\bootstrap\Html::beginForm('', '', ['id' => 'bang-cham-cong-danh_sach'])?>
<div class="row" style="display: flex;align-items: flex-end">
    <div class="col-md-6 " >
        <ul class="list-unstyled ">
            <li>
                <label class="checkbox sang">
                    <input style="width: 12px;height: 12px" type="checkbox" checked disabled/>
                    <span >SÁNG</span>
                </label>
            </li>
            <li>
                <label class="checkbox chieu">
                    <input style="width: 12px;height: 12px" type="checkbox"checked disabled/>
                    <span>CHIỀU</span>
                </label>
            </li>
            <li>
                <label class="checkbox nghi">
                    <input style="width: 12px;height: 12px" type="checkbox"checked disabled/>
                    <span>NGHỈ</span>
                </label>
            </li>
        </ul>
    </div>

    <div class="col-md-1">
        <button type="button" class="btn truoc">Tháng trước</button>
    </div>
    <div class="col-md-1">
        <button type="button" class="btn sau">Tháng sau</button>
    </div>
    <div class="col-md-2">
        <?=\yii\bootstrap\Html::label('Tháng')?>
        <?=Html::dropDownList('month', date("m")-1, ['01','02','03','04','05','06', '07', '08', '09', 10, 11, 12], ['class' => 'form-control','id'=>'thang'])?>
    </div>

    <div class="col-md-2">
        <?=\yii\bootstrap\Html::label('Năm')?>
        <?=Html::textInput('year', date("Y"), ['class' => 'form-control','id'=>'nam'])?>
    </div>

</div>

<?=\yii\bootstrap\Html::endForm()?>

<div id="danh-sach-cham-cong" style="margin-top: 15px"></div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/bang-cham-cong-index.js',[ 'depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
<?php $this->registerCssFile(Yii::$app->request->baseUrl.'/backend/assets/css/bangchamcong.css'); ?>