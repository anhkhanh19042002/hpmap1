<?php
use yii\helpers\Url;
use backend\models\BangChamCong;
/* @var $data_cham_congs [] */
/* @var $days [] */
?>

<div class="table-container">
2    <table class="table table-bordered table-striped text-nowrap table-responsive">
        <thead>
        <tr>
            <th width="1%">STT</th>
            <th>Nhân viên</th>
            <?php foreach ($days as $day): ?>
                <th width="1%" class="text-center"><?= sprintf('%02d', $day)?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php $stt=0?>
        <?php $i = 0; foreach ($data_cham_congs as $index => $data_cham_cong):?>
            <?php $stt++

            ?>
            <tr>
                <td><?=$stt?></td>
                <td> <?= $index ?></td>
                <?php foreach ($data_cham_cong as $item): $i++; ?>
                    <?php
                        if($item['thoi_gian']==date('Y-m-d'))
                        {
                            $ngay= true;
                        }
                        else
                        {
                            $ngay= false;
                        }
                    ?>
                    <td class="<?= isweekend($item['thoi_gian']) == "true" ? 'cot-chan' : '' ?>">
                        <div data-li_do="<?=$item['ly_do_nghi']?>" class="cb-cham-cong" data-value="<?= $item['thoi_gian'].'/'.$item['id_nhan_vien'] ?>">
                            <ul class="list-unstyled " style="display: flex;justify-content: center">
                                <li>
                                    <label class="checkbox sang">
                                        <input type="checkbox" name="cham_cong_<?=$i?>" value="0" <?=$item['he_so']==BangChamCong::SANG||$item['he_so']==BangChamCong::CA_NGAY||$item['he_so']==BangChamCong::CO_PHEP?'checked ':'';  ?> <?=$ngay==false?'disabled':''?> />
                                        <span></span>
                                    </label>
                                </li>
                                <li>
                                        <label class="checkbox chieu">
                                            <input type="checkbox" name="cham_cong_<?=$i?>" value="1" <?=$item['he_so']==BangChamCong::CHIEU||$item['he_so']==BangChamCong::CA_NGAY||$item['he_so']==BangChamCong::CO_PHEP?'checked ':''?> <?=$ngay==false?'disabled':''?>/>
                                            <span></span>
                                        </label>
                                </li>
                                <li>
                                    <label  data-toggle="tooltip" title="Lí do: <?=$item['ly_do_nghi']?>" class="checkbox nghi">
                                        <input  type="checkbox" name="cham_cong_<?=$i?>" value="2" <?=$item['he_so']==BangChamCong::NGHI||$item['he_so']==BangChamCong::CO_PHEP?'checked ':''?> <?=$ngay==false?'disabled':''?>/>
                                        <span></span>
                                    </label>
                                </li>

                            </ul>
                        </div>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <?php
    function isweekend($date){
        $date = strtotime($date);
        $date = date("l", $date);
        $date = strtolower($date);
        if($date == "sunday") {
            return "true";
        } else {
            return "false";
        }
    }
    ?>

    <style>
        .checkbox{
            margin-top:0!important;
            margin-bottom: 0!important;
            padding-top:0!important;
            padding-bottom: 0!important;
        }
        .cot-chan {
            background: rgba(171, 171, 171, 0.56);
        }

        table {
            border-collapse: collapse;
            caption-side: top;
            text-transform: capitalize;
        }
        td, th {
            padding: 10px;
            background: white;
            box-sizing: border-box;
            text-align: left;
        }

        .table-container {
            position: relative;
            max-height: 500px;
            width: 100%;
            overflow: scroll;
        }

        thead th {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            z-index: 2;
        }

        thead th:first-child {
            left: 0;
            z-index: 4;
        }

        tbody {
            overflow: scroll;
            height: 100%;
        }

        tbody td {
            z-index: 2;
        }

        tbody td:first-child {
            z-index: 3;
        }

        /* MAKE LEFT COLUMN FIXEZ */
        tr > :first-child {
            position: -webkit-sticky;
            position: sticky;
            left: 0;
        }
    </style>
</div>
<script>
    $(document).ready(function (){
        $('[data-toggle="tooltip"]').tooltip();
    })
</script>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/plugins/bootstrap.min.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => \yii\web\View::POS_END ]); ?>
