<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\QuanLySanPhamSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $khoang_gias [] */
/* @var $nhan_viens [] */
/* @var $quan_huyens [] */

?>

<div class="category-search">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'chu_nha') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'dien_thoai_chu_nha') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'dia_chi') ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'so_tang') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'huong') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'khoang_gia')->dropDownList($khoang_gias, ['prompt' => '-- Tất cả']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'quan_huyen')->dropDownList($quan_huyens, ['prompt' => '-- Tất cả']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'nhan_vien_phu_trach_id')->dropDownList($nhan_viens, ['prompt' => '-- Tất cả']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'nguoi_tao_id')->dropDownList($nhan_viens, ['prompt' => '-- Tất cả']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'sap_xep_dt')->dropDownList(\backend\models\SanPham::getListSapXep(), ['prompt' => '-- Tất cả']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'sap_xep_gia')->dropDownList(\backend\models\SanPham::getListSapXep(), ['prompt' => '-- Tất cả']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'trang_thai')->dropDownList([
                \backend\models\SanPham::DA_DUYET => \backend\models\SanPham::DA_DUYET,
                \backend\models\SanPham::DANG_BAN => \backend\models\SanPham::DANG_BAN,
            ], ['prompt' => '-- Tất cả']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'sap_xep_ngay')->dropDownList([
                'Ngày mới' => 'Ngày mới',
                'Ngày cũ' => 'Ngày cũ',
            ], ['prompt' => '-- Tất cả']) ?>
        </div>
        <div class="col-md-4">
            <?= \common\models\myAPI::activeDateField2($form, $model, 'tu_ngay', 'Từ ngày', '2000:'.date("Y")) ?>
        </div>
    </div>

    <?php  ?>

</div>
