<?php

use backend\models\search\QuanLySanPhamSearch;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\VarDumper;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel QuanLySanPhamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kho sản phẩm';
$this->params['breadcrumbs'][] = $this->title;
CrudAsset::register($this);

?>
<?php ActiveForm::begin([
    'options' => ['id' => 'form-chon-nhan-vien']
]) ?>
<p>Sản phẩm đã chọn: </p>
<ul id="nhan-vien-duoc-chon" class="list-inline list-unstyled"></ul>
<?php $form = ActiveForm::end(); ?>
<input data-value="Tăng dần" type="hidden" id="sort">
<input  type="hidden" id="data-search">
<div class="san-pham-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'toolbar'=> [
                ['content'=>
                    Html::button('<i class="fa fa-sort"></i> Sắp xếp giá <i class="fa fa-angle-down"></i>',['class'=>'btn blue btn-sort ','data-toggle'=>'dropdown','onclick'=>'Sort()']).
                    Html::a('<i class="fa fa-user-plus"></i> Giao nhân viên', '#', ['title'=> 'Giao nhân viên phụ trách sản phẩm','class'=>'btn yellow btn-giao-nhan-vien-phu-trach']).
                    Html::a('<i class="fa fa-trash"></i> Xóa sản phẩm', '#', ['title'=> 'Xóa sản phẩm','class'=>'btn red btn-xoa-nhieu-san-pham']).
                    '
                     <div style="margin-top: 10px">
                       <input onchange="Search()" id="quanlysanphamsearch-id" class="form-control"  placeholder="Tìm kiếm...">
                     </div>',
                ],
            ],
//            'tableOptions'=>['class'=>'text-nowrap'],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i>Danh sách sản phẩm trong kho',
            ]
        ])?>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",
    'size' => Modal::SIZE_LARGE,
])?>
<?php Modal::end(); ?>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/index-san-pham6.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/js-view/search6.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => View::POS_END ]); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/backend/assets/plugins/bootstrap.min.js', ['depends' => ['backend\assets\Qltk2Asset'], 'position' => View::POS_END ]); ?>
