<?php

use backend\models\QuanLySanPham;
use backend\models\SanPham;
use backend\models\search\QuanLySanPhamSearch;
use yii\helpers\Html;
use common\models\myAPI;
use yii\helpers\VarDumper;
/* @var $searchModel QuanLySanPhamSearch */
return [
    [
        'class' => 'yii\grid\CheckboxColumn',
        'headerOptions' => ['width' => '1%' ],
        'checkboxOptions' => function ($model, $key, $index, $column) {
            /** @var $model QuanLySanPham */
            return [

                'status'=>'0',
                'class' => [(($model->trang_thai == SanPham::DA_DUYET))  ? '' : 'hidden','check'.$model->id ],
                'value' => $model->id,
                'data-value' => (($model->trang_thai == SanPham::DA_DUYET) || ($model->trang_thai == SanPham::KHONG_XAC_NHAN_PHU_TRACH) || ($model->trang_thai == SanPham::CHO_BAN))  ? 1 : 0
            ];
        },
        'contentOptions' => ['class' => 'td-check-don-hang']
    ],

    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary text-success'],
        /** @var $model QuanLySanPham */
        'contentOptions' =>  function($model) {

            return [
                'class'=>$model->id,
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
    ],

    [
        /** @var $data QuanLySanPham */
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-nowrap text-success'],
        'label' => 'Sản phẩm',
        'contentOptions' =>  function($model) {
            return [
                'class'=>$model->id,
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'value'=>function($data){
            $color="";
            if($data->trang_thai==SanPham::DANG_BAN)
            {
                $color="color: #45B6AF";
            }
            elseif ($data->trang_thai==SanPham::DA_DUYET)
            {
                $color="color: orange";
            }
            elseif ($data->trang_thai==SanPham::DA_BAN_MOT_PHAN)
            {
                $color="color: #525e5e";
            }
            return implode('<br/>',[
//                "<script>document.write(screen.width)</script>",

                '<span data-toggle="tooltip" title="'.$data->trang_thai.'" style="'.$color.'">Sản phẩm: '. $data->id.' <i class="fa fa-info-circle"></i></span>',
                '<strong>Tiêu đề: <strong/>' .$data->tieu_de,
                '<strong>Giá: </strong>'. $data->gia_tu.' '.$data->don_vi_tinh,
            ]);
        },
        'format'=>'raw',
    ],

    [
        /** @var $data QuanLySanPham */
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-nowrap text-success'],
        'width' => '1%',
        'label' => 'Thông tin',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'value'=>function($data){
            return implode('<br/>',[
//                "<script>document.write(screen.width)</script>",
                '<strong>Dài: </strong>'.$data->chieu_dai.' m. <br/>
                 <strong>Rộng: </strong>'.$data->chieu_rong.' m',
                '<strong>Diện tích: </strong>'.$data->dien_tich.' m<sup>2</sup>',
            ]);
        },
        'format'=>'raw',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dia_chi',
        'label' => 'Vị tri',
        'headerOptions' => ['width' => '1%','class'=>'text-success '],
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'format'=>'raw',
        'value'=>function($data){
            $link_map=myAPI::GetToaDo($data->id);
            return implode('<br/>', [
                '<strong>Quận/Huyện: </strong>'.$data->quan_huyen,
                '<strong>Phường xã: </strong>'.$data->phuong_xa_id,
                '<strong>Đường rộng: </strong>'.$data->duong,
                '<strong>Hướng: </strong>'.str_replace(',', ', ', $data->huong),
                '<strong>Tọa độ: </strong><a  href='.$link_map.' target="_blank">  <i class="fa fa-map-marker" style="color: red; font-size: 14px"></i></a>',

            ]);
        },
    ],



    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nguoi_phu_trach',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'headerOptions'=>['class'=>'text-success'],
        'width' => '1%',
        'label' => 'Nhân viên',
        'value'=>function($data){
            return implode('<br/>', [
                '<strong>Người phụ trách</strong>',
                $data->nguoi_phu_trach,
                '<strong>Người cập nhật</strong>',
                $data->ho_ten_nguoi_cap_nhat,
            ]);
        },
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'ngay_tao',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'headerOptions'=>['class'=>'text-success'],
        'width' => '1%',
        'label' => 'Ngày',
        'value'=>function($data){
            return implode('<br/>', [

                $data->ngay_tao == '' ? '' : '<strong>Ngày cập nhật:</strong><br>'.date("d/m/Y", strtotime($data->ngay_tao)),
            ]);
        },
        'format'=>'raw',
    ],
    [
        'label' => 'Chức năng',
        'headerOptions' => ['width' => '1%', 'class' => 'text-primary  text-success'],
        'contentOptions' => ['class' => ' text-nowrap '],
        'value' => function($data){
            $str = implode('', [
                    '<ul class="list-unstyled" >',
                    '<li><a target="_blank"  style="color: orange" onclick="XemChiTiet('.$data->nid.')"  data-value="' . $data->id . '"><i class="fa fa-eye"></i> Chi tiết</a></li>',
                    '<li><a class="btn-giao-nhan-vien-phu-trach-san-pham" data-value="'.$data->id.'" ><i class="fa fa-user-plus "></i> Giao nhân viên</a></li>',
                    '<li><a style="color: #0a0a0a" target="_blank" href="https://hpmap.vn/node/'.$data->nid.'/edit"><i class="fa fa-edit"></i> Sửa</a></li>',
                    '<li><a  style="color: red" href="#" class="btn-xoa" data-value="' . $data->id . '"><i class="fa fa-trash"></i> Xóa</a></li>',
                    '</ul>'
                ]);
            return implode('', [
                $str,
            ]);
        },
        'format' => 'raw',
    ],
];

