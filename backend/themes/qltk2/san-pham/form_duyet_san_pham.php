<?php
/**
 * @var $model SanPham
 */

use backend\models\SanPham;
use yii\widgets\ActiveForm; ?>


<?php $form = ActiveForm::begin([
    'options' => ['id' => 'form-duyet-san-pham']
]) ?>
<input type="hidden" name="san_pham_id" id="san_pham_id" value="<?=$model->id?>">
<h4 class="text-primary">THÔNG TIN SẢN PHẨM </h4>
<div class="row">
    <div class="col-md-6">
        <p><strong>Địa chỉ:</strong> <?=$dia_chi?></p>
    </div>
    <div class="col-md-6">
        <p><strong>Địa chỉ chi tiết:</strong> <?=$model->dia_chi_cu_the?></p>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <p><strong>Kích thước:</strong> <?=number_format($model->chieu_dai, 2, ',', '.')?> x <?=number_format($model->chieu_rong, 2, ',', '.')?></p>
    </div>
    <div class="col-md-6">
        <p><strong>Diện tích:</strong> <?=number_format($model->dien_tich, 2, ',', '.')?> m<sup>2</sup></p>
    </div>
</div>
<p><strong>Hướng:</strong> <?=$model->huong?></p>
<div class="row">
    <div class="col-md-3">
        <p><strong>Đường:</strong> <?= is_null($model->duong) ? '' : number_format($model->duong, 2, ',', '.').' m'?></p>
    </div>
    <div class="col-md-3">
        <p><strong>Số tầng:</strong> <?=number_format($model->so_tang, 0, ',', '.')?></p>
    </div>
    <div class="col-md-3">
        <p><strong>Số căn:</strong> <?=number_format($model->so_can, 0, ',', '.')?></p>
    </div>
    <div class="col-md-3">
        <p><strong>Giá:</strong>
            <?= implode(' - ', array_filter([
                    number_format($model->gia_tu, 2, ',', '.'),
                    number_format($model->gia_den, 2, ',', '.')
            ])).' (tỷ)'
            ?>
        </p>
    </div>
</div>
<h4 class="text-primary">DUYỆT SẢN PHẨM</h4>
<hr/>
<div class="row">
    <div class="col-md-6">
        <?=$form->field($model,'trang_thai')->dropDownList([
                SanPham::DA_DUYET => 'Duyệt',
            SanPham::KHONG_DUYET => SanPham::KHONG_DUYET
        ])?>
    </div>
</div>
<?=$form->field($model,'ghi_chu')->textarea(['rows'=>3])?>

<?php ActiveForm::end(); ?>
