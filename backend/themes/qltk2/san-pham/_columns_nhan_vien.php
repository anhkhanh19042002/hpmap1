<?php

use backend\models\QuanLySanPham;
use backend\models\SanPham;
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $searchModel \backend\models\search\QuanLySanPhamSearch */

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary'],
    ],
//    [
//        'label' => 'Chức năng',
//        'headerOptions' => ['width' => '1%', 'class' => 'text-primary'],
//        'value' => function($data){
//            $user = \backend\models\QuanLyUser::find()->andFilterWhere(['id'=>Yii::$app->user->id])->andWhere("vai_tro like '%Trưởng phòng%'")->one();
//            $xoa = $user == null ? '' :
//                '<li><a target="_blank" href="https://hpmap.vn/node/'.$data->nid.'/edit"><i class="fa fa-edit"></i> Sửa</a></li>
//                <li><a href="#" class="btn-xoa" data-value="'.$data->id .'"><i class="fa fa-trash"></i> Xóa</a></li>';
//
//            $str = '<div class="btn-group dropup" style="text-align: inherit">
//                        <button type="button" class="btn btn-primary  dropdown-toggle" data-toggle="dropdown" style="text-align: inherit">
//                        <i class="fa fa-ellipsis-horizontal"></i><i class="fa fa-cogs"></i> Chọn'.' <i class="fa fa-angle-down"></i>
//                        </button>
//                        <ul class="dropdown-menu list-inline" data-value="' . ($data->id) . '">
//                           <li><a href="#" class="btn-chi-tiet" data-value="'.$data->id.'"><i class="fa fa-eye"></i> Chi tiết</a></li>
//                           '.$xoa.'
//                        </ul>
//                    </div>';
//            return $str;
//        },
//        'format' => 'raw'
//    ],

    [
        /** @var $data QuanLySanPham */
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-nowrap text-success'],
        'label' => 'Sản phẩm',
        'contentOptions' =>  function($model) {
            return [
                'class'=>$model->id,
            ];
        },
        'value'=>function($data){
            $color="";
            if($data->trang_thai==SanPham::DANG_BAN)
            {
                $color="color: #45B6AF";
            }
            elseif ($data->trang_thai==SanPham::DA_DUYET)
            {
                $color="color: orange";
            }
            elseif ($data->trang_thai==SanPham::DA_BAN_MOT_PHAN)
            {
                $color="color: #525e5e";
            }
            return implode('<br/>',[
//                "<script>document.write(screen.width)</script>",

                '<span >Sản phẩm: '. $data->id.' </span>',
                '<strong>Tiêu đề: <strong/>' .$data->tieu_de,
                '<strong>Chủ nhà: <strong/>' .$data->chu_nha,
                $data->ngay_tao == '' ? '' : '<strong>Ngày cập nhật: </strong>'.date("d/m/Y", strtotime($data->ngay_tao)),
            ]);
        },
        'format'=>'raw',
    ],
    [
        /** @var $data QuanLySanPham */
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-nowrap text-success'],
        'width' => '1%',
        'label' => 'Thông tin',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'value'=>function($data){
            return implode('<br/>',[
//                "<script>document.write(screen.width)</script>",
                '<strong>Dài: </strong>'.$data->chieu_dai.' m. <br/>
                 <strong>Rộng: </strong>'.$data->chieu_rong.' m',
                '<strong>Diện tích: </strong>'.$data->dien_tich.' m<sup>2</sup>',
            ]);
        },
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dia_chi',
        'label' => 'Vị tri',
        'headerOptions' => ['width' => '1%','class'=>'text-success '],
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'format'=>'raw',
        'value'=>function($data){
            $link_map="https://www.google.com/maps/search/".join("",explode(" ",$data->toa_do_vi_tri));
            return implode('<br/>', [
                '<strong>Quận/Huyện: </strong>'.$data->quan_huyen,
                '<strong>Phường xã: </strong>'.$data->phuong_xa_id,
                '<strong>Đường rộng: </strong>'.$data->duong,
                '<strong>Hướng: </strong>'.str_replace(',', ', ', $data->huong),
                '<strong>Tọa độ: </strong><a href='.$link_map.' target="_blank">  <i class="fa fa-map-marker" style="color: red; font-size: 14px"></i></a>',

            ]);
        },
    ],



    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nguoi_phu_trach',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'headerOptions'=>['class'=>'text-success'],
        'width' => '1%',
        'label' => 'Nhân viên',
        'value'=>function($data){
            return implode('<br/>', [
                '<strong>Người phụ trách</strong>',
                $data->nguoi_phu_trach,
                '<strong>Người cập nhật</strong>',
                $data->ho_ten_nguoi_cap_nhat,
            ]);
        },
        'format'=>'raw',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=> 'trang_thai',
        'headerOptions' => ['width'=>'1%'],
        'value' => function($data){
            /** @var $data QuanLySanPham */
            return $data->trang_thai == SanPham::DA_BAN ? '<span class="text-primary"><i class="fa fa-send"></i> <strong>'.$data->nguoi_ban.'</strong> Đã bán</span>' :  SanPham::$arr_trang_thai[$data->trang_thai];
        },
        'label' => 'Trạng thái',
        'format' => 'raw',
    ],
    [
        'label' => 'Chức năng',
        'headerOptions' => ['width' => '1%', 'class' => 'text-primary  text-success'],
        'contentOptions' => ['class' => ' text-nowrap '],
        'value' => function($data){
            $str = implode('', [
                '<ul class="list-unstyled" >',
                '<li><a style="color: orange" href="#" class="btn-chi-tiet" data-value="'.$data->id.'"><i class="fa fa-eye"></i> Chi tiết</a></li>'.
               '</ul>'
            ]);


            return implode('', [
                $str,
            ]);
        },
        'format' => 'raw',
    ],
];
