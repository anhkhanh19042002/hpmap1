<?php

use backend\models\QuanLySanPham;
use backend\models\SanPham;
use backend\models\search\QuanLySanPhamSearch;
use yii\helpers\Html;

/* @var $searchModel QuanLySanPhamSearch */
return [

    [
        'class' => 'kartik\grid\SerialColumn',
        'header' => 'STT',
        'width' => '1%',
        'headerOptions' => ['class' => 'text-primary text-success'],
        /** @var $model QuanLySanPham */
        'contentOptions' =>  function($model) {
            return [
                'class'=>$model->id,
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
    ],

    [
        /** @var $data QuanLySanPham */
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-nowrap text-success'],
        'label' => 'Sản phẩm',
        'contentOptions' =>  function($model) {
            return [
                'class'=>$model->id,
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'value'=>function($data){
            $color="";
            if($data->trang_thai==SanPham::DANG_BAN)
            {
                $color="color: #45B6AF";
            }
            elseif ($data->trang_thai==SanPham::DA_DUYET)
            {
                $color="color: orange";
            }
            elseif ($data->trang_thai==SanPham::DA_BAN_MOT_PHAN)
            {
                $color="color: #525e5e";
            }
            return implode('<br/>',[
//                "<script>document.write(screen.width)</script>",

                '<span >Sản phẩm: '. $data->id.' </span>',
                '<strong>Tiêu đề: <strong/>' .$data->tieu_de,
                '<strong>Chủ nhà: <strong/>' .$data->chu_nha,
                $data->ngay_tao == '' ? '' : '<strong>Ngày cập nhật: </strong>'.date("d/m/Y", strtotime($data->ngay_tao)),
            ]);
        },
        'format'=>'raw',
    ],

    [
        /** @var $data QuanLySanPham */
        'class'=>'\kartik\grid\DataColumn',
        'headerOptions' => ['class' => 'text-primary text-nowrap text-success'],
        'width' => '1%',
        'label' => 'Thông tin',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'value'=>function($data){
            return implode('<br/>',[
//                "<script>document.write(screen.width)</script>",
                '<strong>Dài: </strong>'.$data->chieu_dai.' m. <br/>
                 <strong>Rộng: </strong>'.$data->chieu_rong.' m',
                '<strong>Diện tích: </strong>'.$data->dien_tich.' m<sup>2</sup>',
            ]);
        },
        'format'=>'raw',
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'dia_chi',
        'label' => 'Vị tri',
        'headerOptions' => ['width' => '1%','class'=>'text-success '],
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'format'=>'raw',
        'value'=>function($data){
            $link_map="https://www.google.com/maps/search/".join("",explode(" ",$data->toa_do_vi_tri));
            return implode('<br/>', [
                '<strong>Quận/Huyện: </strong>'.$data->quan_huyen,
                '<strong>Phường xã: </strong>'.$data->phuong_xa_id,
                '<strong>Đường rộng: </strong>'.$data->duong,
                '<strong>Hướng: </strong>'.str_replace(',', ', ', $data->huong),
                '<strong>Tọa độ: </strong><a href='.$link_map.' target="_blank">  <i class="fa fa-map-marker" style="color: red; font-size: 14px"></i></a>',

            ]);
        },
    ],

    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'nguoi_phu_trach',
        'contentOptions' =>  function($model) {
            return [
                'class'=>[$model->id,'text-nowrap'],
                'onclick'=>'checkBoxKhoSanPham('.$model->id.')',
            ];
        },
        'headerOptions'=>['class'=>'text-success'],
        'width' => '1%',
        'label' => 'Nhân viên',
        'value'=>function($data){
            return implode('<br/>', [
                '<strong>Người phụ trách</strong>',
                $data->nguoi_phu_trach,
                '<strong>Người cập nhật</strong>',
                $data->ho_ten_nguoi_cap_nhat,
            ]);
        },
        'format'=>'raw',
    ]

,
    [
        'label' => 'Chức năng',
        'headerOptions' => ['width' => '1%', 'class' => 'text-primary  text-success'],
        'contentOptions' => ['class' => ' text-nowrap '],
        'value' => function($data){
            $str = implode('', [
                '<ul class="list-unstyled" >',

                '<li><a style="color: #0a0a0a" target="_blank" href="https://hpmap.vn/node/'.$data->nid.'/edit"><i class="fa fa-edit"></i> Sửa</a></li>',
                '<li><a style="color: #0a0a0a"  target="_blank" href="https://hpmap.vn/node/'.$data->nid.'/edit"><i class="fa fa-edit"></i> Sửa</a></li>',
                Yii::$app->user->id == $data->nhan_vien_phu_trach_id ? ($data->trang_thai == SanPham::DANG_BAN ? '<li><a href="#" class="btn-tim-khach" data-value="' . $data->id . '"><i class="fa fa-search"></i> Tìm khách</a></li>' : '') : '',
                Yii::$app->user->id == $data->nhan_vien_phu_trach_id ? ($data->trang_thai == SanPham::DANG_BAN ? '<li><a href="#" class="btn-them-khach" data-value="' . $data->id . '"><i class="fa fa-user-plus"></i> Thêm khách</a></li>' : '') : '',
                Yii::$app->user->id == $data->nhan_vien_phu_trach_id ? ($data->trang_thai == SanPham::CHO_XAC_NHAN_PT ? '<li><a href="#" class="btn-xac-nhan-phu-trach" data-value="' . $data->id . '" data-nid="' . $data->nid . '"><i class="fa fa-check-square"></i> Xác nhận phụ trách</a></li>' : '') : '',
                '<li><a style="color: darkred" href="#" class="btn-huy-san-pham" data-value="' . $data->id . '" data-nid="' . $data->nid . '"><i class="fa fa-ban"></i> Hủy</a></li>',
                '<li><a href="#" class="btn-ban" data-value="' . $data->id . '"><i class="fa fa-money"></i> Bán SP</a></li>',
                '<li><a  style="color: red" href="#" class="btn-xoa" data-value="' . $data->id . '"><i class="fa fa-trash"></i> Xóa</a></li>',

                '</ul>'
            ]);


            return implode('', [
                $str,
            ]);
        },
        'format' => 'raw',
    ],
];

