function loadBangChamCong(){
    $.ajax({
        url: '../bang-cham-cong/load',
        data: $("#bang-cham-cong-danh_sach").serializeArray(),
        dataType: 'json',
        type: 'post',
        beforeSend: function () {
            $("#danh-sach-cham-cong").html('');
            $('.thongbao').html('');
            Metronic.blockUI();
        },
        success: function (data) {
            $("#danh-sach-cham-cong").html(data.content);
        },
        error: function (r1, r2) {
            $('.thongbao').html(r1.responseText);
        },
        complete: function () {
            Metronic.unblockUI();
        }
    });
}

$(document).ready(function () {
    loadBangChamCong();
    $(document).on('click', '.btn-load-du-lieu', function (e) {
        e.preventDefault();
        loadBangChamCong();
    });

    $(document).on('change', '#thang,#nam', function (e) {
        e.preventDefault();
        loadBangChamCong();
    });
    $(document).on('click','.truoc',function (){
        if(parseInt($('#thang').attr('value'))>0)
        {
            $('#thang').attr('value',$('#thang').attr('value')-1);
            $("#thang").val($('#thang').attr('value'));
            loadBangChamCong();
        }
    });
    $(document).on('click','.sau',function (){
        if (parseInt($('#thang').attr('value'))<11)
        {
            $('#thang').attr('value',parseInt($('#thang').attr('value'))+1);
            $("#thang").val($('#thang').attr('value'));
            loadBangChamCong();
        }

    });
    $(document).on('change', 'input[type="checkbox"]', function (e) {
        if($(this).attr('value')==2)
        {
            $('input[name="' + this.name + '"]').not(this).prop('checked', false);
        }
        else {
            $('.nghi input[name="' + this.name + '"]').prop('checked', false);
        }
        var $name=this.name;
        var $check = this.checked;
        var $ly_do_nghi = $(this).attr('data-ly_do');
        var $heso = $(this).attr('value');
        var $checkbox = $(this).parent().parent().parent().parent();
        var $id = $checkbox.attr('data-value');
        if(($heso === '2' ) && $check === true){
            loadForm({id: $id, heso: $heso, type: 'ly_do_nghi'},'l',function (data) {},function () {
                var $loi = 0;
                var $message = '';

                if($loi === 0){
                    SaveObject("../bang-cham-cong/ly-do-nghi", $("#form-ly-do-nghi").serializeArray(), function (data) {
                        if(data.status==1)
                        {
                            $('.sang input[name="' + $name + '"]').prop('checked', true);
                            $('.chieu input[name="' + $name + '"]').prop('checked', true);
                            $('.nghi input[name="' + $name + '"]').parent().attr('data-original-title','Lý do: '+data.ly_do);
                        }
                        else if(data.status==2){
                            $('.nghi input[name="' + $name + '"]').prop('checked', true);
                            $('.nghi input[name="' + $name + '"]').parent().attr('data-original-title','Lý do: '+data.ly_do);
                        }
                        else {
                            $('.nghi input[name="' + $name + '"]').prop('checked', false);
                        }
                    });
                }else {
                    $.alert($message);
                    return false;
                }
            })
        }

        else {
            $.ajax({
                url: '../bang-cham-cong/update-heso',
                data: {heso: $heso, id: $id, check: $check},
                dataType: 'json',
                type: 'post',
                beforeSend: function () {
                    $('.thongbao').html('');
                    Metronic.blockUI();
                },
                success: function (data) {
                },
                error: function (r1, r2) {
                },
                complete: function () {
                    Metronic.unblockUI();
                }
            });
        }
    });
});
