function checkBoxKhoSanPham(id)
{
    if($('.check'+id).attr('status')==0)
    {
        $('.check'+id).prop('checked',true);
        if($('#nhan-vien-duoc-chon li#nhan-vien-'+$('.check'+id).val()).length === 0){
            $("#nhan-vien-duoc-chon").append(`<li id="nhan-vien-`+$('.check'+id).val()+`" class="margin-bottom-10">
                        <label class="label label-primary">
                            <a href="#" class="text-danger btn-remove-nhanvien" data-value="`+$('.check'+id).val()+`"><i class="fa fa-close"></i></a> SP `+$('.check'+id).val()+`
                        </label>
                        <input type="hidden" value="`+$('.check'+id).val() +`" name="NhanVien[`+$('.check'+id).val()+`]">

                    </li>`)
        }
        $('.check'+id).attr('status','1');
    }
    else
    {
        $('.check'+id).prop('checked',false);

        $("#nhan-vien-duoc-chon li#nhan-vien-" + $('.check'+id).val()).remove();

        $('.check'+id).attr('status','0');
    }

}
function Search()
{
    $('#data-search').attr('value',$('#quanlysanphamsearch-id').val());
    $.ajax({
        type: "POST",
        url: '../san-pham/save-search',
        data: { search: $('#data-search').val()},
        dataType: 'json',
        beforeSend: function() {
            Metronic.blockUI();
        },
        success:function( response ) {
            $.pjax.reload({container: "#crud-datatable-pjax"});
            $('#quanlysanphamsearch-id').attr('value',$('#data-search').val())
        },complete: function(response) {
            Metronic.unblockUI();
        },
    });
}
function Sort()
{
    if($('#sort').attr('data-value')=='Giảm dần')
    {

        $('#sort').attr('data-value','Tăng dần');
    }
    else
    if($('#sort').attr('data-value')=='Tăng dần')
    {
        $('#sort').attr('data-value','Giảm dần');
    }
    $.ajax({
        type: "POST",
        url: '../san-pham/save-search',
        data: { value: $('#sort').attr('data-value')},
        dataType: 'json',
        beforeSend: function() {
            Metronic.blockUI();
        },
        success:function( response ) {
            $.pjax.reload({container: "#crud-datatable-pjax"});
        },complete: function(response) {
            Metronic.unblockUI();
        },
    });
}
function XemChiTiet(nid)
{
    $.ajax({
        type: "POST",
        url: '../san-pham/xem-chi-tiet',
        data: { value: nid},
        dataType: 'json',
        beforeSend: function() {
            Metronic.blockUI();
        },
        success:function( response ) {
            window.open(response.content, '_blank');
        },complete: function(response) {
            Metronic.unblockUI();
        },
    });
}
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('#crud-datatable-pjax').on('pjax:success', function() {
        $('#quanlysanphamsearch-tu_ngay').datepicker($.extend({}, $.datepicker.regional['vi'], {"dateFormat":"dd/mm/yy","changeMonth":true,"yearRange":"2015:2025","changeYear":true}));
    });
    $('.btn-toa-do')
    $(document).on('click','.btn-xac-nhan',function (e) {
        e.preventDefault();
        loadForm({san_pham_id: $(this).attr('data-value'),type: 'duyet_san_pham'},'l',function (data) {},function () {
            var $loi = 0;
            var $message = '';

            if ($("#sanpham-trang_thai").val() === ''){
                $message += '<p>Vui lòng chọn trạng thái</p>';
                $loi++;
            }

            if($loi === 0){
                $.ajax({
                    type:'POST',
                    url: '../san-pham/xac-nhan-san-pham',
                    data: $("#form-duyet-san-pham").serializeArray(),
                    beforeSend: function () {
                        Metronic.blockUI();
                    },
                    success: function (data) {
                        alertify.success('Duyệt sản phẩm thành công');
                        if(data.nid !== undefined) {
                            $.ajax({
                                url: 'https://hpmap.vn/update-status-product',
                                type: 'post',
                                data: {status: 1, ids: [data.nid]},
                                beforeSend: function () {
                                    Metronic.blockUI();
                                },
                                success: function (data) {
                                },
                                complete: function () {
                                    $.unblockUI();
                                },
                                error: function (r1, r2) {
                                    Metronic.unblockUI();
                                }
                            });
                        }
                        $.pjax.reload({container: "#crud-datatable-pjax"});
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (r1, r2) {
                        Metronic.unblockUI();
                    }

                });
                // SaveObject("../san-pham/xac-nhan-san-pham", $("#form-duyet-san-pham").serializeArray(), function (data) {
                //         $('#soluongchoduyet').remove();
                //         $('.soluongchoduyet').append('<span class="badge badge-danger">'+data.soluong+'</span>');
                //     if(data.nid !== undefined) {
                //         $.ajax({
                //             url: 'https://hpmap.vn/update-status-product',
                //             type: 'post',
                //             data: {status: 1, ids: [data.nid]},
                //             beforeSend: function () {

                //                 Metronic.blockUI();
                //             },
                //             success: function (data) {
                //             },
                //             complete: function () {
                //                 $.unblockUI();
                //             },
                //             error: function (r1, r2) {
                //                 Metronic.unblockUI();
                //             }
                //         });
                //     }
                //     $.pjax.reload({container: "#crud-datatable-pjax"});
                // });
            }else {
                $.alert($message);
                return false;
            }

        })
    });

    $(document).on('click','.btn-chi-tiet',function (e) {
        e.preventDefault();
        viewData('../san-pham/xem-chi-tiet-san-pham',{id: $(this).attr('data-value')},'xl')
    });

    $(document).on('click','.btn-sua',function (e) {
        e.preventDefault();
        window.location = '../san-pham/update&id='+$(this).attr('data-value');
    });

    $(document).on('click', '.btn-giao-nhan-vien-phu-trach', function (e) {
        e.preventDefault();
        if($("#nhan-vien-duoc-chon li").length === 0)
            $.alert('Vui lòng chọn ít nhất một sản phẩm');
        else{
            var $data = $("#form-chon-nhan-vien").serializeArray();
            $data.push({name: 'type', value: 'giao_nhan_vien_phu_trach'});
            loadForm($data, 'l', function (data) {
            }, function () {
                if($('#nhan-vien-phu-trach').val() === '')  {
                    $.alert('Chưa chọn nhân viên phụ trách');
                    return false;
                }
                else if($('#table-nvien-phu-trach tbody tr').length === 0){
                    $.alert('Không có thông tin sản phẩm');
                    return  false;
                }
                else{
                    $.ajax({
                        type:'POST',
                        url: '../san-pham/luu-nhan-vien-phu-trach',
                        data:  $("#form-giao-nvien-phu-trach").serializeArray(),
                        beforeSend: function () {
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            alertify.success('Giao sản phẩm thành công');
                            $.pjax.reload({container: "#crud-datatable-pjax"});
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (r1, r2) {
                            Metronic.unblockUI();
                        }

                    });
                    // SaveObject('../san-pham/luu-nhan-vien-phu-trach', $("#form-giao-nvien-phu-trach").serializeArray(), function (data) {
                    //     $.pjax.reload({container: "#crud-datatable-pjax"});
                    // });
                }

            })
        }
    });
    $(document).on('click', '.btn-giao-nhan-vien-phu-trach-san-pham', function (e) {
        e.preventDefault();
        var $data = $("#form-chon-nhan-vien").serializeArray();
        $data.push({name: 'type', value: 'giao_nhan_vien_phu_trach-san-pham'});
        $data.push({name: 'id', value: $(this).attr('data-value')});
        loadForm($data, 'l', function (data) {
        }, function () {
            if($('#nhan-vien-phu-trach').val() === '')  {
                $.alert('Chưa chọn nhân viên phụ trách');
                return false;
            }
            else{
                $.ajax({
                    type:'POST',
                    url: '../san-pham/luu-nhan-vien-phu-trach-san-pham',
                    data:  $("#form-giao-nvien-phu-trach-san-pham").serializeArray(),
                    beforeSend: function () {
                        Metronic.blockUI();
                    },
                    success: function (data) {
                        alertify.success('Giao sản phẩm thành công');
                        $.pjax.reload({container: "#crud-datatable-pjax"});
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    error: function (r1, r2) {
                        Metronic.unblockUI();
                    }

                });
                // SaveObject('../san-pham/luu-nhan-vien-phu-trach-san-pham', $("#form-giao-nvien-phu-trach-san-pham").serializeArray(), function (data) {
                //     $.pjax.reload({container: "#crud-datatable-pjax"});
                // });
            }

        })

    });

    $(document).on('click', '.btn-xoa-nv-phu-trach', function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    })

    // xử lý chọn nhiều sản phẩm
    $(document).on('change', '.check-item.check-don-hang', function (e) {
        var $selected = false;
        if($(this).is(':checked'))
            $selected = true;
        if($selected){
            console.log('flag');
            $(this).prop('checked', true);
            if($('#nhan-vien-duoc-chon li#nhan-vien-'+$(this).val()).length === 0){
                $("#nhan-vien-duoc-chon").append(`<li id="nhan-vien-`+$(this).val()+`" class="margin-bottom-10">
                        <label class="label label-primary">
                            <a href="#" class="text-danger btn-remove-nhanvien" data-value="`+$(this).val()+`"><i class="fa fa-close"></i></a> Mã SP `+$(this).val()+`
                        </label>
                        <input type="hidden" value="`+$(this).val() +`" name="NhanVien[`+$(this).val()+`]">
                    </li>`)
            }
        }else
            $("#nhan-vien-duoc-chon li#nhan-vien-"+$(this).val()).remove();
    });

    $(document).on('click', '.btn-remove-nhanvien', function (e) {
        e.preventDefault();
        const value = $(this).attr('data-value');
        $("input[value="+value+"]").prop('checked', false);
        $(this).parent().parent().remove();
    });

    $("#crud-datatable-pjax").on('pjax:success', function() {
        $(document).on('change', '.select-on-check-all', function () {
            var $selected = false;
            if($(this).is(':checked'))
                $selected = true;
            $(".td-check-don-hang input").each(function () {
                if($selected){
                    $(this).prop('checked', true);
                    if($('#nhan-vien-duoc-chon li#nhan-vien-'+$(this).val()).length === 0){
                        $("#nhan-vien-duoc-chon").append(`<li id="nhan-vien-`+$(this).val()+`">
                            <label class="label label-primary">
                                <a href="#" class="text-danger btn-remove-nhanvien" data-value="`+$(this).val()+`"><i class="fa fa-close"></i></a> SP `+$(this).val()+`
                            </label>
                            <input type="hidden" name="NhanVien[`+$(this).val()+`]" value="`+$(this).val()+`">
                        </li>`)
                    }
                }
                else{
                    $(this).prop('checked', false);
                    $("#nhan-vien-duoc-chon li#nhan-vien-"+$(this).val()).remove();
                }
            });
        });
    });

    $(document).on('change', '.td-check-don-hang input', function (e) {
        var $selected = false;
        if($(this).is(':checked'))
            $selected = true;
        if($selected){
            if($('#nhan-vien-duoc-chon li#nhan-vien-'+$(this).val()).length === 0){
                $("#nhan-vien-duoc-chon").append(`<li id="nhan-vien-`+$(this).val()+`" class="margin-bottom-10">
                        <label class="label label-primary">
                            <a href="#" class="text-danger btn-remove-nhanvien" data-value="`+$(this).val()+`"><i class="fa fa-close"></i></a> SP `+$(this).val()+`
                        </label>
                        <input type="hidden" value="`+$(this).val() +`" name="NhanVien[`+$(this).val()+`]">
                    </li>`)
            }
        }else {
            $("#nhan-vien-duoc-chon li#nhan-vien-" + $(this).val()).remove();
        }
    });

    $(document).on('click','.btn-luu-san-pham',function (e) {
        e.preventDefault();
        var $message = '';
        var $loi = 0;
        $(".help-block").html('');
        if($loi === 0){
            var data = new FormData($('#form-san-pham')[0]);
            SaveObjectUploadFile("../san-pham/save", data, function (data) {
                $("#form-san-pham").html('');
            });
        }else{
            $.alert($message);
        }
    });

    $(document).on('click', '.btn-xoa', function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        SaveObject('../san-pham/xoa', {id: $id}, function (data) {
                            $.ajax({
                                url: 'https://hpmap.vn/update-status-product',
                                type: 'post',
                                data: {status: 'Không duyệt', ids: [data.id]},
                                beforeSend: function () {
                                    Metronic.blockUI();
                                },
                                success: function (data) {
                                },
                                complete: function () {
                                    $.unblockUI();
                                },
                                error: function (r1, r2) {
                                    Metronic.unblockUI();
                                }
                            });
                            $.pjax({container: "#crud-datatable-pjax"});
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-san-pham-da-ban', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-san-pham-da-ban');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-san-pham-dang-ban', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-san-pham-dang-ban');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-kho-san-pham-da-ban', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-kho-san-pham-da-ban');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-kho-san-pham-dang-ban', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-kho-san-pham-dang-ban');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-kho-san-pham-da-duyet', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-kho-san-pham-da-duyet');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-kho-san-pham', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-kho-san-pham');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-san-pham-dang-ban2', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-san-pham-dang-ban-giam-doc');
        });
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-san-pham-da-ban2', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-san-pham-da-ban-giam-doc');
        });
    });

    $(document).on('click','.btn-ban-lai',function (e) {
        e.preventDefault();
        loadForm({san_pham_id: $(this).attr('data-value'),type: 'duyet_san_pham_ban_lai'},'l',function (data) {},function () {
            var $loi = 0;
            var $message = '';
            if ($('#sanpham-trang_thai').val() === ''){
                $message += '<p>Vui lòng chọn trạng thái</p>';
                $loi++;
            }

            if($loi === 0){
                SaveObject("../san-pham/xac-nhan-ban-lai", $("#form-xac-nhan-ban-lai").serializeArray(), function (data) {
                    $.ajax({
                        url: 'https://hpmap.vn/update-status-product',
                        type: 'post',
                        data: {status: 0, ids: data.arr},
                        beforeSend: function () {
                            Metronic.blockUI();
                        },
                        success: function (data) {
                            $.pjax.reload({container: "#crud-datatable-pjax"});
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        error: function (r1, r2) {
                            Metronic.unblockUI();
                        }
                    });
                });
            }else {
                $.alert($message);
                return false;
            }
        })
    });

    $(document).on('click','.btn-sua-lich-su-doanh-thu',function (e) {
        e.preventDefault();
        loadForm({lich_su_id: $(this).attr('data-value'),type: 'cap_nhat_lich_su_doanh_thu'},'l',function (data) {},function () {
            var $loi = 0;
            var $message = '';

            if ($('#lichsudoanhthu-doanh_thu_hoa_hong').val() === ''){
                $message += '<p>Vui lòng nhập doanh thu hoa hồng!</p>';
                $loi++;
            }
            if ($('#lichsudoanhthu-ngay_thanh_toan').val() === ''){
                $message += '<p>Vui lòng chọn ngày thanh toán!</p>';
                $loi++;
            }
            if ($('#lichsudoanhthu-nguoi_thanh_toan').val() === ''){
                $message += '<p>Vui lòng nhập người thanh toán!</p>';
                $loi++;
            }

            if($loi === 0){
                SaveObject("../san-pham/cap-nhat-doanh-thu", $("#form-update-thong-tin-ban").serializeArray(), function (data) {
                    $(".xem-chi-tiet-doanh-thu-hoa-hong").html(data.data);
                });
            }else {
                $.alert($message);
                return false;
            }
        })
    });

    $(document).on('click', '.btn-xoa-lich-su-doanh-thu', function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        SaveObject('../san-pham/xoa-lich-su-doanh-thu', {id: $id}, function (data) {
                            $(".xem-chi-tiet-doanh-thu-hoa-hong").html(data.data);
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });

    $(document).on('click','.btn-import', function (e) {
        e.preventDefault();
        loadForm2({type: 'import'}, 'm', function (data) {
        }, function () {
            var data = new FormData($('#form-import')[0]);
            SaveObjectUploadFile('../san-pham/import', data, function (data) {

                $.pjax.reload({container: "#crud-datatable-pjax"});

            })

        }, '<i class="fa fa-print"></i> Thực hiện import hội viên');
    });

    $(document).on('click', '.btn-huy-san-pham', function (e) {
        e.preventDefault();
        var $id = $(this).attr('data-value');
        $.alert({
            title: 'Thông báo',
            icon: 'fa fa-warning',
            type: 'red',
            text: 'Bạn có chắc chắn muốn thực hiện việc này?',
            content:'Bạn có chắc chắn muốn thực hiện việc này?',
            buttons: {
                btnAccept: {
                    text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                    action: function () {
                        SaveObject('../san-pham/huy-san-pham', {id: $id}, function (data) {
                            $.pjax({container: "#crud-datatable-pjax"});
                        })
                    },
                    btnClass: 'btn-primary'
                },
                btnCancel: {
                    text: '<i class="fa fa-ban"></i> Huỷ'
                }
            }
        })
    });

    $(document).ready(function () {
        $(document).on('click', '.btn-download-san-pham-dang-ban2', function (e) {
            e.preventDefault();
            taiFileExcel('../san-pham/tai-danh-sach-san-pham-dang-ban-giam-doc');
        });
    });

    $(document).on('click','.btn-download-san-pham-tim-kiem',function (e) {
        e.preventDefault();
        loadForm({type: 'tai-excel-kho-san-pham'},'m',function (data) {},function () {
            var $loi = 0;
            var $message = '';

            if($loi == 0){
                taiFileExcel('../san-pham/tai-danh-sach-kho-san-pham',$("#form-tai-excel").serializeArray());

            }else {
                $.alert($message);
                return false;
            }

        })
    });

    $(document).on('click', '.btn-update-nv-cap-nhat-phap-ly', function (e) {
        e.preventDefault();
        if($('#nhan-vien-duoc-chon li').length === 0)
            $.alert('Vui lòng chọn ít nhất 1 sản phẩm');
        else{
            var $data = $("#form-chon-nhan-vien").serializeArray();
            $data.push({name: 'type', value: 'update_nv_cap_nhat_phap_ly'});
            loadForm($data, 'xl', function (data) {

            }, function () {
                SaveObject('../san-pham/update-nv-cap-nhat-phap-ly', $("#form-cap-nhat-nv-cap-nhat-phap-ly").serializeArray(), function (data) {
                    $.pjax.reload({container: "#crud-datatable-pjax"});
                    $("#nhan-vien-duoc-chon").html('');
                })
            })
        }
    })

    $(document).on('click', '.btn-da-ban-san-pham', function (e) {
        e.preventDefault();
        loadForm({san_pham_id: $(this).attr('data-value'),type: 'da_ban_san_pham'},'l',function (data) {},function () {

            SaveObject("../san-pham/da-ban-san-pham", $("#form-xac-nhan-da-ban").serializeArray(), function (data) {
                $.pjax.reload({container: "#crud-datatable-pjax"});
            });
        })
    });

    $(document).on('change','#sap_xep_dt',function (e) {
        e.preventDefault();

        if($('#sap_xep_dt').val() === 'Mặc định'){

        }
        $.pjax.reload({container: '#crud-datatable-pjax', data: {sx_dien_tich: $('#sap_xep_dt').val()}});
    });

    $(document).on('click','.btn-update-thong-tin-ban',function (e) {
        e.preventDefault();
        loadForm({san_pham_id: $(this).attr('data-value'),type: 'update_thong_tin_ban'},'l',function (data) {},function () {
            var $loi = 0;
            var $message = '';

            if ($('#lichsudoanhthu-doanh_thu_hoa_hong').val() === ''){
                $message += '<p>Vui lòng nhập doanh thu hoa hồng!</p>';
                $loi++;
            }
            if ($('#lichsudoanhthu-ngay_thanh_toan').val() === ''){
                $message += '<p>Vui lòng chọn ngày thanh toán!</p>';
                $loi++;
            }
            if ($('#lichsudoanhthu-nguoi_thanh_toan').val() === ''){
                $message += '<p>Vui lòng nhập người thanh toán!</p>';
                $loi++;
            }

            if($loi === 0){
                SaveObject("../san-pham/luu-thong-tin-ban", $("#form-update-thong-tin-ban").serializeArray(), function (data) {
                    $.pjax.reload({container: "#crud-datatable-pjax"});
                });
            }else {
                $.alert($message);
                return false;
            }
        })
    });

    $(document).on('click', '.xoa-anh-san-pham', function (e) {
        e.preventDefault();
        var $idSP = $(this).attr('data-value');
        var $myCol = $(this).parent().parent();

        $.confirm({
            content: 'Bạn có chắc chắn xoá ảnh này không?',
            text: 'Thông báo',
            icon: 'fa fa-question',
            title: 'Xoá ảnh sản phẩm',
            type: 'blue',
            buttons: {
                btnOK: {
                    text: '<i class="fa fa-check"></i> Chấp nhận',
                    btnClass: 'btn-primary',
                    action: function () {
                        viewData('../san-pham/xoa-anh', {idSanPham: $idSP}, 'm', function () {
                            $myCol.remove();
                        });
                    }
                },
                btnClose: {
                    text: '<i class="fa fa-close"></i> Huỷ và đóng lại'
                }
            }
        });
    });

    $(document).on('click', '.btn-xem-khach-hang-tiem-nang', function (e) {
        e.preventDefault();
        var $sanPham = $(this).attr('data-value');
        viewData('../san-pham/view-khach-hang-tiem-nang', {san_pham: $sanPham}, 'xl', function (data) {
        });
    });
    $(document).on('click','.btn-xoa-khach-hang',function (e) {
        e.preventDefault();
        var id = $(this).attr('data-value');
        var mytr = $(this).parent().parent();
        $.confirm({
            content: 'Bạn có chắc chắn xoá khách hàng này không?',
            text: 'Thông báo',
            icon: 'fa fa-question',
            title: 'Xoá khách hàng',
            type: 'red',
            buttons: {
                btnOK: {
                    text: '<i class="fa fa-check"></i> Chấp nhận',
                    btnClass: 'btn-primary',
                    action: function () {
                        viewData('../san-pham/xoa-khach-hang', {id: id}, 'm', function () {
                            mytr.remove();
                        });
                    }
                },
                btnClose: {
                    text: '<i class="fa fa-close"></i> Huỷ và đóng lại'
                }
            }
        })
    });

    $(document).on('click','.btn-ban',function (e) {
        e.preventDefault();
        loadForm({san_pham_id: $(this).attr('data-value'),type: 'ban_san_pham'},'l',function (data) {},function () {
            var $loi = 0;
            var $message = '';

            if ($('#chon-nguoi-ban-hang').val() === ''){
                $message += '<p>Vui lòng người bán</p>';
                $loi++;
            }
            if ($('#thongtinbanhang-ngay_ban').val() === ''){
                $message += '<p>Vui lòng nhập ngày bán</p>';
                $loi++;
            }

            if($loi === 0){
                SaveObject("../san-pham/luu-thong-tin-ban-hang", $("#form-ban-hang").serializeArray(), function (data) {
                    $.pjax.reload({container: "#crud-datatable-pjax"});
                });
            }else {
                $.alert($message);
                return false;
            }

        })
    });

    $(document).on('change','#chon-nguoi-ban-hang',function (e) {
        if ($(this).val() === 'Nhân viên') {
            $("#block-nhan-vien-ban").removeClass('hidden');
            $("#block-ho-ten-nguoi-ban").addClass('hidden');
        }else{
            $("#block-ho-ten-nguoi-ban").removeClass('hidden');
            $("#block-nhan-vien-ban").addClass('hidden');
            if($(this).val() === 'Chủ nhà'){
                $("#thongtinbanhang-nguoi_ban").val($("#ho-ten-chu-nha").val());
                $("#thongtinbanhang-nguoi_ban").prop("readonly",true);
            }
            else if ($(this).val() === 'Đơn vị khác'){
                $("#thongtinbanhang-nguoi_ban").val('');
                $("#thongtinbanhang-nguoi_ban").prop("readonly",false);

            }
        }
    });

    $(document).on('click', '.btn-xoa-nhieu-san-pham',function (e) {
        e.preventDefault();
        if($("#nhan-vien-duoc-chon li").length === 0)
            $.alert('Vui lòng chọn ít nhất một sản phẩm');
        else {
            var $data = $("#form-chon-nhan-vien").serializeArray();

            $.alert({
                title: 'Thông báo',
                icon: 'fa fa-warning',
                type: 'red',
                text: 'Bạn có chắc chắn muốn thực hiện việc này?',
                content: 'Bạn có chắc chắn muốn thực hiện việc này?',
                buttons: {
                    btnAccept: {
                        text: '<i class="fa fa-check-circle-o"></i> Đồng ý',
                        action: function () {
                            SaveObject('../san-pham/xoa-nhieu-san-pham', $data, function (data) {
                                $.ajax({
                                    url: `https://hpmap.vn/update-status-user?uid=${data.uid}`,
                                    type: 'get',
                                    beforeSend: function () {
                                        Metronic.blockUI();
                                    },
                                    success: function (data) {
                                    },
                                    complete: function () {
                                        $.unblockUI();
                                    },
                                    error: function (r1, r2) {
                                        Metronic.unblockUI();
                                    }
                                });
                                $.pjax({container: "#crud-datatable-pjax"});
                            });
                        },
                        btnClass: 'btn-primary'
                    },
                    btnCancel: {
                        text: '<i class="fa fa-ban"></i> Huỷ'
                    }
                }
            });
        }
    });

    $(document).on('click', '.btn-gia-han',function (e) {
        e.preventDefault();
        const id = $(this).attr('data-value');

        loadForm({type: 'gia_han_san_pham', id},'l',function (data) {},function () {
            let loi = 0
            let message = '';

            if($('#giaodich-so_tien').val() === ''){
                loi++;
                message += '<p>Vui lòng nhập số tiền</p>'
            }

            if($('#ngay-gia-han').val() === ''){
                loi++;
                message += '<p>Vui lòng chọn ngày gia hạn</p>'
            }

            if(loi === 0){
                SaveObject("../san-pham/luu-gia-han", $("#form-gia-han").serializeArray(), function (data) {});
            }else{
                if(message !== ''){
                    $.alert(message);
                }
                return false;
            }
        });
    });
})
