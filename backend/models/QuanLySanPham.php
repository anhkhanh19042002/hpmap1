<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int|null $quan_id
 * @property int|null $duong_pho_id
 * @property int|null $phuong_xa_id
 * @property string|null $phuong_xa
 * @property string|null $dia_chi
 * @property string|null $dia_chi_cu_the
 * @property string|null $dien_thoai_chu_nha
 * @property string|null $chu_nha
 * @property float|null $chieu_dai
 * @property float|null $chieu_rong
 * @property float|null $dien_tich
 * @property float|null $so_tang
 * @property int|null $so_can
 * @property string|null $huong
 * @property float|null $duong ĐV: mét
 * @property float|null $gia_tu ĐV: Tỉ
 * @property int|null $nhan_vien_ban_id
 * @property string|null $nguoi_ban
 * @property string|null $ghi_chu
 * @property string|null $trang_thai
 * @property string|null $created
 * @property int|null $user_id
 * @property int|null $active
 * @property float|null $gia_den
 * @property string|null $ngay_tao
 * @property int|null $nhan_vien_phu_trach_id
 * @property int|null $khach_hang_id
 * @property string|null $type_nguoi_ban
 * @property string|null $ghi_chu_ban_import
 * @property string|null $phap_ly
 * @property int|null $nguoi_tao_id
 * @property string|null $khoang_gia
 * @property string|null $khoang_dien_tich
 * @property float|null $doanh_thu_hoa_hong
 * @property float|null $hoa_hong_nhan_vien
 * @property string|null $ngay_thanh_toan
 * @property string|null $nguoi_thanh_toan
 * @property float|null $loi_nhuan
 * @property float|null $hoa_hong_nhap_hang
 * @property string|null $link_video
 * @property float|null $da_thanh_toan_nhap_hang
 * @property float|null $da_thanh_toan_ban_hang
 * @property float|null $con_lai_nhap_hang
 * @property float|null $con_lai_ban_hang
 * @property float|null $doanh_thu_da_lay
 * @property float|null $doanh_thu_con_lai
 * @property float|null $hoa_hong_chuyen_khach
 * @property int|null $chuyen_khach_id
 * @property string|null $ngay_ban
 * @property float|null $con_lai_hoa_hong_chuyen_khach
 * @property float|null $da_lay_hoa_hong_chuyen_khach
 * @property int|null $nid
 * @property string|null $tieu_de
 * @property int|null $thanh_pho_id
 * @property string|null $danh_sach_cac_dinh
 * @property string|null $toa_do_vi_tri
 * @property int|null $so_giuong
 * @property int|null $so_phong_tam
 * @property string|null $han_dang
 * @property string|null $duong_pho
 * @property string|null $quan_huyen
 * @property string|null $nguoi_phu_trach
 * @property string|null $anh_phu_trach
 * @property string|null $ho_ten_nguoi_cap_nhat
 * @property int $so_luong_khach_co_nhu_cau
 * @property string|null $phan_loai
 * @property string|null $don_vi_tinh
 * @property integer|null $vip
 */

class QuanLySanPham extends ActiveRecord
{
    public $sap_xep_dt;
    public $tu_ngay;
    public $den_ngay;
    public $sx_dien_tich;
    public $sap_xep_gia;
    public $sap_xep_ngay;
    public $tu_khoa;


    public static function tableName()
    {
        return '{{%quan_ly_san_pham}}';
    }

    public function rules()
    {
        return [
            [['id', 'quan_id', 'duong_pho_id','phuong_xa_id','phuong_xa' ,'so_can', 'nhan_vien_ban_id', 'user_id', 'active', 'nhan_vien_phu_trach_id',
                'khach_hang_id', 'nguoi_tao_id', 'chuyen_khach_id', 'nid', 'thanh_pho_id', 'so_giuong', 'so_phong_tam',
                'so_luong_khach_co_nhu_cau', 'khoang_dien_tich', 'phan_loai', 'han_dang', 'vip'], 'safe'],
            [['chieu_dai', 'chieu_rong', 'dien_tich', 'so_tang', 'duong', 'gia_tu', 'gia_den', 'doanh_thu_hoa_hong',
                'hoa_hong_nhan_vien', 'loi_nhuan', 'hoa_hong_nhap_hang', 'da_thanh_toan_nhap_hang', 'da_thanh_toan_ban_hang',
                'con_lai_nhap_hang', 'con_lai_ban_hang', 'doanh_thu_da_lay', 'doanh_thu_con_lai', 'hoa_hong_chuyen_khach',
                'con_lai_hoa_hong_chuyen_khach', 'da_lay_hoa_hong_chuyen_khach'], 'number'],
            [['huong', 'ghi_chu', 'trang_thai', 'type_nguoi_ban', 'ghi_chu_ban_import', 'phap_ly', 'khoang_gia', 'link_video',
                'danh_sach_cac_dinh', 'toa_do_vi_tri'], 'safe'],
            [['created', 'ngay_tao', 'ngay_thanh_toan', 'ngay_ban'], 'safe'],
            [['dia_chi', 'nguoi_thanh_toan'], 'safe'],
            [['dia_chi_cu_the'], 'safe'],
            [['dien_thoai_chu_nha'], 'safe'],
            [['chu_nha', 'nguoi_ban', 'duong_pho', 'quan_huyen', 'nguoi_phu_trach', 'ho_ten_nguoi_cap_nhat'], 'safe'],
            [['tieu_de', 'anh_phu_trach','don_vi_tinh_id'], 'safe'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quan_id' => 'Quan ID',
            'duong_pho_id' => 'Duong Pho ID',
            'dia_chi' => 'Địa chỉ',
            'dia_chi_cu_the' => 'Dia Chi Cu The',
            'dien_thoai_chu_nha' => 'Điện thoại chủ nhà',
            'chu_nha' => 'Chủ nhà',
            'chieu_dai' => 'Chieu Dai',
            'chieu_rong' => 'Chieu Rong',
            'dien_tich' => 'Dien Tich',
            'so_tang' => 'Số tầng',
            'so_can' => 'So Can',
            'huong' => 'Hướng',
            'duong' => 'Duong',
            'gia_tu' => 'Gia Tu',
            'nhan_vien_ban_id' => 'Nhan Vien Ban ID',
            'nguoi_ban' => 'Nguoi Ban',
            'ghi_chu' => 'Ghi Chu',
            'trang_thai' => 'Trạng thái',
            'created' => 'Created',
            'user_id' => 'User ID',
            'active' => 'Active',
            'gia_den' => 'Gia Den',
            'ngay_tao' => 'Ngay Tao',
            'nhan_vien_phu_trach_id' => 'Nhân viên phụ trách',
            'khach_hang_id' => 'Khach Hang ID',
            'type_nguoi_ban' => 'Type Nguoi Ban',
            'ghi_chu_ban_import' => 'Ghi Chu Ban Import',
            'phap_ly' => 'Phap Ly',
            'nguoi_tao_id' => 'Người tạo',
            'khoang_gia' => 'Khoảng giá',
            'doanh_thu_hoa_hong' => 'Doanh Thu Hoa Hong',
            'hoa_hong_nhan_vien' => 'Hoa Hong Nhan Vien',
            'ngay_thanh_toan' => 'Ngay Thanh Toan',
            'nguoi_thanh_toan' => 'Nguoi Thanh Toan',
            'loi_nhuan' => 'Loi Nhuan',
            'hoa_hong_nhap_hang' => 'Hoa Hong Nhap Hang',
            'link_video' => 'Link Video',
            'da_thanh_toan_nhap_hang' => 'Da Thanh Toan Nhap Hang',
            'da_thanh_toan_ban_hang' => 'Da Thanh Toan Ban Hang',
            'con_lai_nhap_hang' => 'Con Lai Nhap Hang',
            'con_lai_ban_hang' => 'Con Lai Ban Hang',
            'doanh_thu_da_lay' => 'Doanh Thu Da Lay',
            'doanh_thu_con_lai' => 'Doanh Thu Con Lai',
            'hoa_hong_chuyen_khach' => 'Hoa Hong Chuyen Khach',
            'chuyen_khach_id' => 'Chuyen Khach ID',
            'ngay_ban' => 'Ngay Ban',
            'con_lai_hoa_hong_chuyen_khach' => 'Con Lai Hoa Hong Chuyen Khach',
            'da_lay_hoa_hong_chuyen_khach' => 'Da Lay Hoa Hong Chuyen Khach',
            'nid' => 'Nid',
            'tieu_de' => 'Tieu De',
            'thanh_pho_id' => 'Thanh Pho ID',
            'danh_sach_cac_dinh' => 'Danh Sach Cac Dinh',
            'toa_do_vi_tri' => 'Toa Do Vi Tri',
            'so_giuong' => 'So Giuong',
            'so_phong_tam' => 'So Phong Tam',
            'han_dang' => 'Han Dang',
            'duong_pho' => 'Duong Pho',
            'quan_huyen' => 'Quận huyện',
            'nguoi_phu_trach' => 'Nguoi Phu Trach',
            'anh_phu_trach' => 'Anh Phu Trach',
            'ho_ten_nguoi_cap_nhat' => 'Ho Ten Nguoi Cap Nhat',
            'so_luong_khach_co_nhu_cau' => 'So Luong Khach Co Nhu Cau',
            'phan_loai' => 'Phan Loai',
            'don_vi_tinh' => 'Don Vi Tinh',
            'vip' => 'Vip',
            'sap_xep_dt'=>'Sắp xếp diện tích',
            'sap_xep_gia'=>'Sắp xếp giá',
            'sap_xep_ngay'=>'Sắp xếp ngày'
        ];
    }
}
