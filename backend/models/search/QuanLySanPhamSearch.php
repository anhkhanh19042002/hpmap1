<?php

namespace backend\models\search;

use backend\models\QuanLySanPham;
use common\models\myAPI;
use common\models\User;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SanPham;
use yii\helpers\VarDumper;

class QuanLySanPhamSearch extends QuanLySanPham
{
    public function rules()
    {
        return [
            [['id', 'quan_id', 'duong_pho_id', 'so_can', 'nhan_vien_ban_id', 'user_id', 'active', 'nhan_vien_phu_trach_id',
                'khach_hang_id', 'nguoi_tao_id', 'so_luong_khach_co_nhu_cau'], 'safe'],
            [['chieu_dai', 'chieu_rong', 'dien_tich', 'so_tang', 'duong', 'gia_tu', 'gia_den'], 'safe'],
            [['huong', 'ghi_chu', 'trang_thai', 'type_nguoi_ban', 'ghi_chu_ban_import', 'phap_ly'], 'safe'],
            [['created', 'ngay_tao','sap_xep_dt','sap_xep_gia','khoang_gia','ngay_ban'], 'safe'],
            [['dia_chi', 'het_han', 'han_dang'], 'safe'],
            [['dia_chi_cu_the','tu_ngay','den_ngay','sx_dien_tich'], 'safe'],
            [['dien_thoai_chu_nha','sap_xep_ngay'], 'safe'],
            [['chu_nha', 'nguoi_ban', 'duong_pho', 'quan_huyen', 'nguoi_phu_trach', 'ho_ten_nguoi_cap_nhat'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_tao'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['or',['like','trang_thai',SanPham::DA_DUYET],['like','trang_thai',SanPham::DANG_BAN],['like','trang_thai',SanPham::DA_BAN_MOT_PHAN]]);

        if($this->tu_ngay){
            $query->andFilterWhere(['>=', 'date(ngay_tao)', myAPI::convertDateSaveIntoDb($this->tu_ngay)]);
        }
        if($this->den_ngay){
            $query->andFilterWhere(['<=', 'date(ngay_tao)', myAPI::convertDateSaveIntoDb($this->den_ngay)]);
        }

        if ($this->sap_xep_ngay == 'Ngày mới'){
            $query->orderBy(['ngay_tao'=>SORT_DESC]);
        }elseif ($this->sap_xep_ngay == 'Ngày cũ'){
            $query->orderBy(['ngay_tao'=>SORT_ASC]);
        }

        if ($this->sap_xep_dt == 'Giảm dần'){
            $query->orderBy(['dien_tich'=>SORT_DESC]);
        }elseif ($this->sap_xep_dt == 'Tăng dần'){
            $query->orderBy(['dien_tich'=>SORT_ASC]);
        }

        if ($this->sap_xep_gia == 'Giảm dần'){
            $query->orderBy(['gia_tu'=>SORT_DESC]);
        }elseif ($this->sap_xep_gia == 'Tăng dần'){
            $query->orderBy(['gia_tu'=>SORT_ASC]);
        }


        $query->andFilterWhere([
//            'id' => $this->id,
            'nhan_vien_phu_trach_id' => $this->nhan_vien_phu_trach_id,
            'nguoi_tao_id' => $this->nguoi_tao_id,
            'so_tang' => $this->so_tang,
        ]);

        $query->andFilterWhere(['or',
            ['like', 'dia_chi', $this->id],
            ['like', 'tieu_de', $this->id],
            ['like', 'dien_thoai_chu_nha', $this->id],
            ['like', 'chu_nha', $this->id],
            ['like', 'huong', $this->id],
            ['like', 'ghi_chu', $this->id   ],
        ]);
        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'id', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'nguoi_phu_trach', $this->nguoi_phu_trach])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'khoang_gia', $this->khoang_gia])
        ;

        return $dataProvider;
    }

    public function searchChoDuyet($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_tao'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['trang_thai' => SanPham::CHO_DUYET]);
        if(!User::isUpdateAll())
            $query->andFilterWhere(['user_id' => Yii::$app->user->id]);

        if ($this->sap_xep_dt == 'Giảm dần'){
            $query->orderBy(['dien_tich'=>SORT_DESC]);
        }elseif ($this->sap_xep_dt == 'Tăng dần'){
            $query->orderBy(['dien_tich'=>SORT_ASC]);
        }
        if ($this->sap_xep_gia == 'Giảm dần'){
            $query->orderBy(['gia_tu'=>SORT_DESC]);
        }elseif ($this->sap_xep_gia == 'Tăng dần'){
            $query->orderBy(['gia_tu'=>SORT_ASC]);
        }

        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'khoang_gia', $this->khoang_gia])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat]);

        return $dataProvider;
    }

    public function searchPhuTrach($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_tao'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if(!User::isViewAll())
            $query->andFilterWhere([
                'nhan_vien_phu_trach_id' => Yii::$app->user->id,
            ]);

        if ($this->sap_xep_dt == 'Giảm dần'){
            $query->orderBy(['dien_tich'=>SORT_DESC]);
        }elseif ($this->sap_xep_dt == 'Tăng dần'){
            $query->orderBy(['dien_tich'=>SORT_ASC]);
        }

        if ($this->sap_xep_gia == 'Giảm dần'){
            $query->orderBy(['gia_tu'=>SORT_DESC]);
        }elseif ($this->sap_xep_gia == 'Tăng dần'){
            $query->orderBy(['gia_tu'=>SORT_ASC]);
        }

        $query->andFilterWhere(['in', 'trang_thai', [SanPham::DANG_BAN, SanPham::CHO_XAC_NHAN_PT, SanPham::DA_BAN_MOT_PHAN]])
            ->andWhere('nhan_vien_phu_trach_id is not null');

        $query->andFilterWhere([
            'id' => $this->id,
            'nhan_vien_phu_trach_id' => $this->nhan_vien_phu_trach_id,
            'nguoi_tao_id' => $this->nguoi_tao_id,
        ]);

        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat])
            ->andFilterWhere(['like', 'khoang_gia', $this->khoang_gia])
            ->andFilterWhere(['like', 'nguoi_phu_trach', $this->nguoi_phu_trach]);
        return $dataProvider;
    }

    public function searchDaBan($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_ban'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!User::isViewAll()){
            $query->andFilterWhere([
                'nhan_vien_ban_id' => Yii::$app->user->id,
            ]);
        }
        if ($this->sap_xep_dt == 'Giảm dần'){
            $query->orderBy(['dien_tich'=>SORT_DESC]);
        }elseif ($this->sap_xep_dt == 'Tăng dần'){
            $query->orderBy(['dien_tich'=>SORT_ASC]);
        }

        $query->andFilterWhere(['or', ['trang_thai' => SanPham::DA_BAN], ['trang_thai' => SanPham::DA_BAN_MOT_PHAN]]);

        if ($this->sap_xep_gia == 'Giảm dần'){
            $query->orderBy(['gia_tu'=>SORT_DESC]);
        }elseif ($this->sap_xep_gia == 'Tăng dần'){
            $query->orderBy(['gia_tu'=>SORT_ASC]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'quan_id' => $this->quan_id,
            'duong_pho_id' => $this->duong_pho_id,
            'chieu_dai' => $this->chieu_dai,
            'chieu_rong' => $this->chieu_rong,
            'so_tang' => $this->so_tang,
            'so_can' => $this->so_can,
            'duong' => $this->duong,
            'nhan_vien_ban_id' => $this->nhan_vien_ban_id,
            'created' => $this->created,
        ]);


        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'khoang_gia', $this->khoang_gia])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat]);

        return $dataProvider;
    }

    public function searchPhuTrachTatCa($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_tao'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'active' => 1,
        ]);
        $query->andWhere('trang_thai = :t or trang_thai = :t2', [
            ':t' => SanPham::DANG_BAN,
            ':t2' => SanPham::CHO_XAC_NHAN_PT
        ]);

        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat]);
        return $dataProvider;
    }

    public function searchDaBanTatCa($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_tao'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'quan_id' => $this->quan_id,
            'duong_pho_id' => $this->duong_pho_id,
            'chieu_dai' => $this->chieu_dai,
            'chieu_rong' => $this->chieu_rong,
            'dien_tich' => $this->dien_tich,
            'so_tang' => $this->so_tang,
            'so_can' => $this->so_can,
            'duong' => $this->duong,
            'gia_tu' => $this->gia_tu,
            'nhan_vien_ban_id' => $this->nhan_vien_ban_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere([
            'trang_thai' => SanPham::DA_BAN,
            'active' => 1,
        ]);

        $query->andWhere('nguoi_ban is not null');

        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat]);


        return $dataProvider;
    }

    public function searchDaDuyetTatCa($params)
    {
        $query = QuanLySanPham::find()->andFilterWhere(['active' => 1])->orderBy(['ngay_tao'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'active' => 1,
        ]);
        $query->andWhere('trang_thai = :t', [
            ':t' => SanPham::DA_DUYET,
        ]);

        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat]);
        return $dataProvider;
    }

    public function searchHetHan($params){
        $query = QuanLySanPham::find()
            ->andFilterWhere(['active' => 0, 'het_han' => 1])->orderBy(['ngay_tao' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->tu_ngay){
            $query->andFilterWhere(['>=', 'date(ngay_tao)', myAPI::convertDateSaveIntoDb($this->tu_ngay)]);
        }
        if($this->den_ngay){
            $query->andFilterWhere(['<=', 'date(ngay_tao)', myAPI::convertDateSaveIntoDb($this->den_ngay)]);
        }

        if ($this->sap_xep_ngay == 'Ngày mới'){
            $query->orderBy(['ngay_tao'=>SORT_DESC]);
        }elseif ($this->sap_xep_ngay == 'Ngày cũ'){
            $query->orderBy(['ngay_tao'=>SORT_ASC]);
        }

        if ($this->sap_xep_dt == 'Giảm dần'){
            $query->orderBy(['dien_tich'=>SORT_DESC]);
        }elseif ($this->sap_xep_dt == 'Tăng dần'){
            $query->orderBy(['dien_tich'=>SORT_ASC]);
        }

        if ($this->sap_xep_gia == 'Giảm dần'){
            $query->orderBy(['gia_tu'=>SORT_DESC]);
        }elseif ($this->sap_xep_gia == 'Tăng dần'){
            $query->orderBy(['gia_tu'=>SORT_ASC]);
        }


        $query->andFilterWhere([
            'id' => $this->id,
            'nhan_vien_phu_trach_id' => $this->nhan_vien_phu_trach_id,
            'nguoi_tao_id' => $this->nguoi_tao_id,
            'so_tang' => $this->so_tang,
        ]);


        $query->andFilterWhere(['like', 'dia_chi', $this->dia_chi])
            ->andFilterWhere(['like', 'dia_chi_cu_the', $this->dia_chi_cu_the])
            ->andFilterWhere(['like', 'dien_thoai_chu_nha', $this->dien_thoai_chu_nha])
            ->andFilterWhere(['like', 'chu_nha', $this->chu_nha])
            ->andFilterWhere(['like', 'huong', $this->huong])
            ->andFilterWhere(['like', 'nguoi_ban', $this->nguoi_ban])
            ->andFilterWhere(['like', 'ghi_chu', $this->ghi_chu])
            ->andFilterWhere(['like', 'nguoi_phu_trach', $this->nguoi_phu_trach])
            ->andFilterWhere(['like', 'ho_ten_nguoi_cap_nhat', $this->ho_ten_nguoi_cap_nhat])
            ->andFilterWhere(['like', 'quan_huyen', $this->quan_huyen])
            ->andFilterWhere(['like', 'trang_thai', $this->trang_thai])
            ->andFilterWhere(['like', 'khoang_gia', $this->khoang_gia])
        ;

        return $dataProvider;
    }
}
