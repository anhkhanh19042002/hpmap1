<?php

namespace backend\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%trang_thai_nghi}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int $so_lan_nghi_con_lai
 * @property int $thang
 * @property string $update_ngay_nghi
 *
 * @property User $user
 */
class TrangThaiNghi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%trang_thai_nghi}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'so_lan_nghi_con_lai'], 'integer'],
            [['so_lan_nghi_con_lai'], 'required'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'so_lan_nghi_con_lai' => 'So Lan Nghi Con Lai',
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
