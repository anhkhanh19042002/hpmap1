Cấu hình Email

Email được lưu trữ trên web hosting nếu cấu hình hoặc lưu trữ trên email hosting
vì khó quá lý và truy cập email hosting nên ta dùng phần mềm quản lý như thunderbird hoặc outlook

- Cấu hình dns:
- Tạo tài khoản email trên DA
- đăng nhập, login bằng outlook (incoming, outcoming)

password: GZzxb9eG

SELECT KHOA.Makhoa,KHOA.TenKhoa,SUM(CHI_TIET_KHAM.Vienphi) AS 'TongVienPhi'
FROM KHOA JOIN BAC_SI on KHOA.Makhoa = BAC_SI.Makhoa 
        JOIN CHI_TIET_KHAM on CHI_TIET_KHAM.Mabs = BAC_SI.Mabs
GROUP BY KHOA.Makhoa, KHOA.TenKhoa;

CREATE VIEW BacSiKhamNhieuNhat as
SELECT TOP 1 BAC_SI.Mabs as 'mabs', BAC_SI.Tenbs as 'tenbs', KHOA.Tenkhoa as 'tenkhoa', count(CHI_TIET_KHAM.id) as 'sobenhnhan'
FROM BAC_SI JOIN KHOA on BAC_SI.Makhoa = KHOA.Makhoa
        JOIN CHI_TIET_KHAM on CHI_TIET_KHAM.Mabs = BAC_SI.Mabs
GROUP BY BAC_SI.Mabs, BAC_SI.Tenbs, KHOA.Tenkhoa
ORDER BY count(CHI_TIET_KHAM.id) DESC;
